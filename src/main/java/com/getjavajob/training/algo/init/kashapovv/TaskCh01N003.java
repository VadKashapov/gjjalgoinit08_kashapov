package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

/**
 * Created by Вадим  on 27.10.2016.
 */
public class TaskCh01N003 {
    public static void main(String[] args) {
        int input = readConsoleInt();

        System.out.println("Input number is " + input);
    }

    public static int readConsoleInt() {
        Scanner in = new Scanner(System.in);
        return in.nextInt();
    }
}
