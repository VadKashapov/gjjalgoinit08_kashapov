package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.util.ConsoleIO.readConsoleDouble;
import static java.lang.Math.*;


/**
 * Created by Вадим on 27.10.2016.
 */
public class TaskCh01N017 {
    public static void main(String[] args) {
        System.out.println("Type x,a,b,c with enter after every value");
        double x = readConsoleDouble();
        double a = readConsoleDouble();
        double b = readConsoleDouble();
        double c = readConsoleDouble();

        System.out.println("Expression O result: " + calcExpO(x));
        System.out.println("Expression P result: " + calcExpP(a, b, c, x));
        System.out.println("Expression R result: " + calcExpR(x));
        System.out.println("Expression S result: " + calcExpS(x));
    }

    public static double calcExpO(double x) {
        return sqrt(1 - pow(sin(x), 2));
    }

    public static double calcExpP(double a, double b, double c, double x) {
        return sqrt(1 / sqrt(a * pow(x, 2) + b * x + c));
    }

    public static double calcExpR(double x) {
        return sqrt(x - 1) + sqrt(x - 1) / (2 * sqrt(x));
    }

    public static double calcExpS(double x) {
        return abs(x) + abs(x + 1);
    }


}
