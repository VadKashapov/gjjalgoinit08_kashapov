package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.util.ConsoleIO.readConsoleInt;

/**
 * Created by Вадим on 28.10.2016.
 */
public class TaskCh02N013 {
    public static void main(String[] args) {
        int num = readConsoleInt();

        int revNum = reverse(num);

        System.out.println(revNum);
    }

    public static int reverse(int num) {
        if (num <= 100 || num >= 200) {
            return 0;
        }
        int revNum = 0;
        int numCopy = num;
        while (numCopy != 0) {
            revNum = revNum * 10 + numCopy % 10;
            numCopy /= 10;
        }
        return revNum;
    }
}
