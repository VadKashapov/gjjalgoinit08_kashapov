package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh02N013.reverse;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 28.10.2016.
 */
public class TaskCh02N013Test {
    public static void main(String[] args) {
        testReverse();
    }

    public static void testReverse() {
        assertEquals("TaskCh02N013Test.testReverse", 21, reverse(120));
        assertEquals("TaskCh02N013Test.testReverse199", 991, reverse(199));
        assertEquals("TaskCh02N013Test.testReverse1999", 0, reverse(1999));
        assertEquals("TaskCh02N013Test.testReverse99", 0, reverse(99));
    }
}
