package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.util.ConsoleIO.readConsoleInt;

/**
 * Created by Вадим on 28.10.2016.
 */
public class TaskCh02N031 {
    public static void main(String[] args) {
        System.out.println("Input number");
        int num = readConsoleInt();

        int changedNum = changeDigitPos(num);

        System.out.println("New num is = " + changedNum);
    }

    /**
     * Method swap last & pre-last position of three-digit number
     **/
    public static int changeDigitPos(int num) {
        if (num < 100 || num > 999) {
            return 0;
        }
        int x = 0;
        int numCopy = num;

        while (numCopy > 9) {
            x = x * 10 + numCopy % 10;
            numCopy /= 10;
        }
        x += numCopy * 100;
        return x;
    }
}
