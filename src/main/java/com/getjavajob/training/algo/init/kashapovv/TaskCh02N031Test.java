package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh02N031.changeDigitPos;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 28.10.2016.
 */
public class TaskCh02N031Test {
    public static void main(String[] args) {
        testChangeDigitPos();
    }

    public static void testChangeDigitPos() {
        assertEquals("TaskCh02N031Test.testChangeDigitPos.251", 251, changeDigitPos(215));
        assertEquals("TaskCh02N031Test.testChangeDigitPos.105", 150, changeDigitPos(105));
        assertEquals("TaskCh02N031Test.testChangeDigitPos.99", 0, changeDigitPos(99));
        assertEquals("TaskCh02N031Test.testChangeDigitPos.999", 0, changeDigitPos(999));
    }

}
