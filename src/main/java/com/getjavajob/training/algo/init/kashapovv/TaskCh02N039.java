package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.util.ConsoleIO.readConsoleInt;

/**
 * Created by Вадим on 29.10.2016.
 */
public class TaskCh02N039 {
    public static void main(String[] args) {
        System.out.println("Input h, m, s (enter after every value)");
        int hours = readConsoleInt();
        int minutes = readConsoleInt();
        int seconds = readConsoleInt();

        double angle = findAngle(hours, minutes, seconds);

        System.out.println("Angle found is " + angle);
    }

    public static double findAngle(int hours, int minutes, int seconds) {
        if (hours < 0 || hours > 23 || minutes < 0 || minutes > 59 || seconds < 0 || seconds > 59) {
            return -1;
        }
        int timeInSec = 3600 * hours + 60 * minutes + seconds;
        final double angleScale = 1.0 / 120.0;
        return timeInSec * angleScale;
    }
}
