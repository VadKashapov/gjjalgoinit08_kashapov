package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh02N039.findAngle;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 29.10.2016.
 */
public class TaskCh02N039Test {
    public static void main(String[] args) {
        testAngleFind();
    }

    public static void testAngleFind() {
        assertEquals("TaskCh02N039Test.testAngle.2min", 1, findAngle(0, 2, 0));
        assertEquals("TaskCh02N039Test.testAngle.3hrs", 90, findAngle(3, 0, 0));
        assertEquals("TaskCh02N039Test.testAngle.9min30sec", 4.75, findAngle(0, 9, 30));
        assertEquals("TaskCh02N039Test.testAngle.notValid", -1, findAngle(0, 120, 30));
        assertEquals("TaskCh02N039Test.testAngle.notValid", -1, findAngle(60, 0, 30));
        assertEquals("TaskCh02N039Test.testAngle.notValid", -1, findAngle(0, 0, 360));
    }
}
