package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

/**
 * Created by Вадим on 29.10.2016.
 */
public class TaskCh02N043 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Input a, b value (enter after every value)");
        int a = in.nextInt();
        int b = in.nextInt();

        int result = checkDivisible(a, b);

        System.out.println("Result is " + result);
    }

    public static int checkDivisible(int a, int b) {
        if (a == 0 || b == 0) {
            return -1;
        } else {
            return a % b == 0 || b % a == 0 ? 1 : 0;
        }
    }
}
