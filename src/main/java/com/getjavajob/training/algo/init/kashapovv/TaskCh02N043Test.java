package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh02N043.checkDivisible;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 29.10.2016.
 */
public class TaskCh02N043Test {
    public static void main(String[] args) {
        testDivisible();
        testNotDivisible();
        testDivisibleException();
    }

    public static void testDivisible() {
        assertEquals("TaskCh02N043Test.testDivisible", 1, checkDivisible(10, 120));
        assertEquals("TaskCh02N043Test.testDivisible", 1, checkDivisible(120, 20));
        assertEquals("TaskCh02N043Test.testDivisible", -1, checkDivisible(0, 0));
    }

    public static void testNotDivisible() {
        assertEquals("TaskCh02N043Test.testNotDivisible", 0, checkDivisible(13, 12));
        assertEquals("TaskCh02N043Test.testNotDivisible", 0, checkDivisible(15, 13));
    }

    public static void testDivisibleException() {
        assertEquals("TaskCh02N043Test.testDivisibleNotValid", -1, checkDivisible(0, 0));
    }
}
