package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

/**
 * Created by Вадим on 30.10.2016.
 */
public class TaskCh03N029 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int x = in.nextInt();
        int y = in.nextInt();
        int z = in.nextInt();

        boolean exprAresult = calcExpressionA(x, y);
        boolean exprBresult = calcExpressionB(x, y);
        boolean exprVresult = calcExpressionV(x, y);
        boolean exprGresult = calcExpressionG(x, y, z);
        boolean exprDresult = calcExpressionD(x, y, z);
        boolean exprEresult = calcExpressionE(x, y, z);

        System.out.println("Expression A " + exprAresult);
        System.out.println("Expression B " + exprBresult);
        System.out.println("Expression V " + exprVresult);
        System.out.println("Expression G " + exprGresult);
        System.out.println("Expression D " + exprDresult);
        System.out.println("Expression E " + exprEresult);
    }

    /**
     * @param x - logic parameter x;
     * @param y - logic parameter y;
     * @return return true if every parameter is odd;
     */
    public static boolean calcExpressionA(int x, int y) {
        return x % 2 != 0 && y % 2 != 0;
    }

    /**
     * @param x - logic parameter x;
     * @param y - logic parameter y;
     * @return return true if only one of parameter less 20;
     */
    public static boolean calcExpressionB(int x, int y) {
        return x < 20 ^ y < 20;
    }

    /**
     * @param x - logic parameter x;
     * @param y - logic parameter y;
     * @return return true if at least one of parameter is 0;
     */
    public static boolean calcExpressionV(int x, int y) {
        return x == 0 || y == 0;
    }

    /**
     * @param x - logic parameter x;
     * @param y - logic parameter y;
     * @param z - logic parameter z;
     * @return return true if every parameter is negative;
     */
    public static boolean calcExpressionG(int x, int y, int z) {
        return x < 0 && y < 0 && z < 0;
    }

    /**
     * @param x - logic parameter x;
     * @param y - logic parameter y;
     * @param z - logic parameter z;
     * @return return true if only one of parameter folding 5;
     */
    public static boolean calcExpressionD(int x, int y, int z) {
        boolean isFoldX = x % 5 == 0;
        boolean isFoldY = y % 5 == 0;
        boolean isFoldZ = z % 5 == 0;
        return isFoldX ^ isFoldY ^ isFoldZ && !(isFoldX && isFoldY && isFoldZ);
    }

    /**
     * @param x - logic parameter x;
     * @param y - logic parameter y;
     * @param z - logic parameter z;
     * @return return true if at least one of parameter more 100;
     */
    public static boolean calcExpressionE(int x, int y, int z) {
        return x > 100 || y > 100 || z > 100;
    }
}
