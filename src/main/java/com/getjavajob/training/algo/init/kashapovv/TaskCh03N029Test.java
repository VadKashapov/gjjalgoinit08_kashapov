package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh03N029.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 30.10.2016.
 */
public class TaskCh03N029Test {
    public static void main(String[] args) {
        testConditionA();
        testConditionB();
        testConditionV();
        testConditionG();
        testConditionD();
        testConditionE();
    }

    public static void testConditionA() {
        assertEquals("TaskCh03N029Test.testConditionA", true, calcExpressionA(1, 1));
        assertEquals("TaskCh03N029Test.testConditionA", false, calcExpressionA(1, 2));
        assertEquals("TaskCh03N029Test.testConditionA", false, calcExpressionA(2, 1));
        assertEquals("TaskCh03N029Test.testConditionA", false, calcExpressionA(2, 2));
    }

    public static void testConditionB() {
        assertEquals("TaskCh03N029Test.testConditionB", false, calcExpressionB(1, 1));
        assertEquals("TaskCh03N029Test.testConditionB", true, calcExpressionB(1, 30));
        assertEquals("TaskCh03N029Test.testConditionB", true, calcExpressionB(30, 1));
        assertEquals("TaskCh03N029Test.testConditionB", false, calcExpressionB(30, 30));
    }

    public static void testConditionV() {
        assertEquals("TaskCh03N029Test.testConditionV", true, calcExpressionV(0, 0));
        assertEquals("TaskCh03N029Test.testConditionV", true, calcExpressionV(0, 1));
        assertEquals("TaskCh03N029Test.testConditionV", true, calcExpressionV(1, 0));
        assertEquals("TaskCh03N029Test.testConditionV", false, calcExpressionV(1, 1));
    }

    public static void testConditionG() {
        assertEquals("TaskCh03N029Test.testConditionG", false, calcExpressionG(1, 1, 1));
        assertEquals("TaskCh03N029Test.testConditionG", false, calcExpressionG(1, 1, -1));
        assertEquals("TaskCh03N029Test.testConditionG", false, calcExpressionG(1, -1, -1));
        assertEquals("TaskCh03N029Test.testConditionG", false, calcExpressionG(-1, 1, 1));
        assertEquals("TaskCh03N029Test.testConditionG", false, calcExpressionG(-1, 1, -1));
        assertEquals("TaskCh03N029Test.testConditionG", false, calcExpressionG(-1, -1, 1));
        assertEquals("TaskCh03N029Test.testConditionG", true, calcExpressionG(-1, -1, -1));
    }

    public static void testConditionD() {
        assertEquals("TaskCh03N029Test.testConditionD", false, calcExpressionD(1, 1, 1));
        assertEquals("TaskCh03N029Test.testConditionD", true, calcExpressionD(1, 1, 5));
        assertEquals("TaskCh03N029Test.testConditionD", true, calcExpressionD(1, 5, 1));
        assertEquals("TaskCh03N029Test.testConditionD", false, calcExpressionD(1, 5, 5));
        assertEquals("TaskCh03N029Test.testConditionD", true, calcExpressionD(5, 1, 1));
        assertEquals("TaskCh03N029Test.testConditionD", false, calcExpressionD(5, 1, 5));
        assertEquals("TaskCh03N029Test.testConditionD", false, calcExpressionD(5, 5, 1));
        assertEquals("TaskCh03N029Test.testConditionD", false, calcExpressionD(5, 5, 5));
    }

    public static void testConditionE() {
        assertEquals("TaskCh03N029Test.testConditionE", false, calcExpressionE(0, 0, 0));
        assertEquals("TaskCh03N029Test.testConditionE", true, calcExpressionE(0, 0, 101));
        assertEquals("TaskCh03N029Test.testConditionE", true, calcExpressionE(0, 101, 0));
        assertEquals("TaskCh03N029Test.testConditionE", true, calcExpressionE(0, 101, 101));
        assertEquals("TaskCh03N029Test.testConditionE", true, calcExpressionE(101, 0, 0));
        assertEquals("TaskCh03N029Test.testConditionE", true, calcExpressionE(101, 0, 101));
        assertEquals("TaskCh03N029Test.testConditionE", true, calcExpressionE(101, 101, 0));
        assertEquals("TaskCh03N029Test.testConditionE", true, calcExpressionE(101, 101, 101));
    }
}
