package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

/**
 * Created by Вадим on 30.10.2016.
 */
public class TaskCh04N015 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int todayMonth = in.nextInt();
        int todayYear = in.nextInt();
        int birthMonth = in.nextInt();
        int birthYear = in.nextInt();

        int foundAge = findAge(todayMonth, todayYear, birthMonth, birthYear);

        System.out.println("Age is " + foundAge);
    }

    public static int findAge(int todayMonth, int todayYear, int birthMonth, int birthYear) {
        if (todayMonth < 0 || todayMonth > 12 || todayYear < birthYear || birthMonth < 0 || birthMonth > 12) {
            return -1;
        }
        int age = todayYear - birthYear;
        return todayMonth < birthMonth ? age - 1 : age;
    }
}
