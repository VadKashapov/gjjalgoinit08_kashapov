package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh04N015.findAge;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 30.10.2016.
 */
public class TaskCh04N015Test {
    public static void main(String[] args) {
        testFindAge();
    }

    public static void testFindAge() {
        assertEquals("TaskCh04N015Test.testBirthMonth > TodayMonth", 29, findAge(12, 2014, 6, 1985));
        assertEquals("TaskCh04N015Test.testBirthMonth < TodayMonth", 28, findAge(5, 2014, 6, 1985));
        assertEquals("TaskCh04N015Test.testBirthMonth = TodayMonth", 29, findAge(6, 2014, 6, 1985));
        assertEquals("TaskCh04N015Test.Exception", -1, findAge(6, 2014, 24, 1985));
    }
}
