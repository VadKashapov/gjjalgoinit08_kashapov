package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

/**
 * Created by Вадим on 30.10.2016.
 */
public class TaskCh04N033 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int natNum = in.nextInt();

        boolean even = isEven(natNum);
        boolean odd = isOdd(natNum);

        System.out.println("Number is even? " + even);
        System.out.println("Number is odd? " + odd);
    }

    public static boolean isEven(int natNum) {
        if (natNum < 1) {
            System.out.print("not natural");
            return false;
        }
        return natNum % 2 == 0;
    }

    public static boolean isOdd(int natNum) {
        if (natNum < 1) {
            System.out.print("not natural");
            return false;
        }
        return natNum % 2 != 0;
    }
}
