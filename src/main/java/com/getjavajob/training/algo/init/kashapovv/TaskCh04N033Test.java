package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh04N033.isEven;
import static com.getjavajob.training.algo.init.kashapovv.TaskCh04N033.isOdd;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 30.10.2016.
 */
public class TaskCh04N033Test {
    public static void main(String[] args) {
        testEven();
        testOdd();
    }

    public static void testEven() {
        assertEquals("TaskCh04N033Test.testEven.True", true, isEven(18));
        assertEquals("TaskCh04N033Test.testEven.False", false, isEven(15));
    }

    public static void testOdd() {
        assertEquals("TaskCh04N033Test.testOdd.True", true, isOdd(17));
        assertEquals("TaskCh04N033Test.testOdd.False", false, isOdd(16));
    }
}
