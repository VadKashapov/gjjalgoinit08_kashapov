package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.util.ConsoleIO.isNatural;
import static com.getjavajob.training.algo.util.ConsoleIO.readConsoleInt;

/**
 * Created by Вадим on 30.10.2016.
 */
public class TaskCh04N036 {
    public static void main(String[] args) {
        int time = readConsoleInt();

        String color = findGlowColor(time); // find color by cycle time intervals;

        System.out.println("Color is " + color);
    }

    public static String findGlowColor(int time) {
        if (isNatural(time)) {
            return time % 5 < 3 ? "green" : "red"; // return color by 3- or 2- minute coming one after another intervals;
        } else {
            return "not valid input";
        }
    }
}
