package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh04N036.findGlowColor;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 30.10.2016.
 */
public class TaskCh04N036Test {
    public static void main(String[] args) {
        testFindGlowColor();
    }

    public static void testFindGlowColor() {
        assertEquals("TaskCh04N036Test.testFindGlowColor.Red", "red", findGlowColor(3));
        assertEquals("TaskCh04N036Test.testFindGlowColor.Green", "green", findGlowColor(5));
        assertEquals("TaskCh04N036Test.testFindGlowColor.NotNatural", "not valid input", findGlowColor(-9));
    }
}
