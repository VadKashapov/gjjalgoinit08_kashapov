package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.util.ConsoleIO.readConsoleInt;

/**
 * Created by Вадим on 30.10.2016.
 */
public class TaskCh04N067 {
    public static void main(String[] args) {
        int dayNum = readConsoleInt();

        String dayType = findDayType(dayNum); // find working or weekend day type is;

        System.out.println("Input day is " + dayType);
    }

    public static String findDayType(int dayNum) {
        if (dayNum < 1) {
            return "not valid input";
        }
        int weekDayNum = dayNum % 7;
        return weekDayNum >= 1 && weekDayNum <= 5 ? "Workday" : "Weekend";
    }
}