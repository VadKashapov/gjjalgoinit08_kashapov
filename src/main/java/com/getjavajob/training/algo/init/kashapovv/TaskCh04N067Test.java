package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh04N067.findDayType;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 30.10.2016.
 */
public class TaskCh04N067Test {
    public static void main(String[] args) {
        testDayType();
    }

    public static void testDayType() {
        assertEquals("TaskCh04N067Test.testDayType.26", "Workday", findDayType(26)); // test for workday day number;
        assertEquals("TaskCh04N067Test.testDayType.2", "Workday", findDayType(2)); // test for workday day number;
        assertEquals("TaskCh04N067Test.testDayType.3", "Workday", findDayType(3)); // test for workday day number;
        assertEquals("TaskCh04N067Test.testDayType.4", "Workday", findDayType(4)); // test for workday day number;
        assertEquals("TaskCh04N067Test.testDayType.7", "Weekend", findDayType(7)); // test for weekend day number;
        assertEquals("TaskCh04N067Test.testDayType.6", "Weekend", findDayType(6)); // test for weekend day number;
        assertEquals("TaskCh04N067Test.testDayType.27", "Weekend", findDayType(27)); // test for weekend day number;
        assertEquals("TaskCh04N067Test.testDayType.NotValid", "not valid input", findDayType(-9));
    }
}
