package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.util.ConsoleIO.isNatural;
import static com.getjavajob.training.algo.util.ConsoleIO.readConsoleInt;

/**
 * Created by Вадим on 30.10.2016.
 */

public class TaskCh04N106 {
    public static void main(String[] args) {
        int monthNum = readConsoleInt();

        String season = findSeason(monthNum);

        System.out.println("Season is " + season);
    }

    public static String findSeason(int monthNum) {
        if (!isNatural(monthNum) || monthNum > 12) {
            return "not valid input";
        }
        switch (monthNum % 12 / 3) {
            case 0:
                return "Winter";
            case 1:
                return "Spring";
            case 2:
                return "Summer";
            case 3:
                return "Autumn";
            default:
                return "";
        }
    }
}
