package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh04N106.findSeason;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 30.10.2016.
 */
public class TaskCh04N106Test {
    public static void main(String[] args) {
        testFindSeason();
    }

    public static void testFindSeason() {
        assertEquals("TaskCh04N106Test.testFindSeason.April", "Spring", findSeason(4));
        assertEquals("TaskCh04N106Test.testFindSeason.December", "Winter", findSeason(12));
        assertEquals("TaskCh04N106Test.testFindSeason.June", "Summer", findSeason(6));
        assertEquals("TaskCh04N106Test.testFindSeason.September", "Autumn", findSeason(9));
        assertEquals("TaskCh04N106Test.testFindSeason.notValid", "not valid input", findSeason(13));
    }
}
