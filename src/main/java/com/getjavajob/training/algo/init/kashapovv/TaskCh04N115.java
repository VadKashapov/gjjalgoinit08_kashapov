package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.util.ConsoleIO.isNatural;
import static com.getjavajob.training.algo.util.ConsoleIO.readConsoleInt;


/**
 * Created by Вадим on 30.10.2016.
 */
public class TaskCh04N115 {
    public static void main(String[] args) {
        System.out.println("Input year >= 1984");
        int year = readConsoleInt();

        String yearCalendarNameFrom1984 = findYearCalendarNameFrom1984(year);
        System.out.println("Calendar name is " + yearCalendarNameFrom1984);

        System.out.println("Input any year ");
        year = readConsoleInt();

        String yearCalendarName = findYearCalendarName(year);
        System.out.println("Calendar name is " + yearCalendarName);


    }

    /**
     * Method find East-calendar animal and color name by year.
     * Calendar has 60 year cycle separated by 12 year sub-cycle,
     * where each one has own animal name.
     * In the same time, animal has color which varies every 2 years independently;
     * There are 5 colors;
     *
     * @param year must be more or equal 1984;
     * @return string consists animal and color name;
     */
    public static String findYearCalendarNameFrom1984(int year) {
        if (year < 1984) {
            return "not valid year";
        }

        final int yearCount = year - 1984;

        int yearResidue = yearCount % 12;
        String animalName = findAnimalName(yearResidue); // return animal name by it number;

        yearResidue = yearCount % 10;
        String colorName = findColorName(yearResidue); // return color name by it number;

        return colorName + ' ' + animalName; // return full calendar name;
    }

    /**
     * Method is analogue findYearCalendarNameFrom1984 but start from first year;
     */
    public static String findYearCalendarName(int year) {
        if (!isNatural(year)) {
            return "not valid year";
        }
        int yearResidue = year;

        yearResidue %= 12;
        String animalName = findAnimalName(yearResidue); // return animal name by it number;

        yearResidue %= 10;
        String colorName = findColorName(yearResidue); // return color name by it number;

        return colorName + ' ' + animalName; // return full calendar name;
    }

    public static String findAnimalName(int yearResidue) {
        switch (yearResidue) {
            case 0:
                return "Rat";
            case 1:
                return "Cow";
            case 2:
                return "Tiger";
            case 3:
                return "Rabbit";
            case 4:
                return "Dragon";
            case 5:
                return "Eel";
            case 6:
                return "Horse";
            case 7:
                return "Sheep";
            case 8:
                return "Monkey";
            case 9:
                return "Cock";
            case 10:
                return "Dog";
            case 11:
                return "Pig";
            default:
                return "";
        }
    }

    public static String findColorName(int yearResidue) {
        switch (yearResidue) {
            case 0:
            case 1:
                return "Green";
            case 2:
            case 3:
                return "Red";
            case 4:
            case 5:
                return "Yellow";
            case 6:
            case 7:
                return "White";
            case 8:
            case 9:
                return "Black";
            default:
                return "";
        }
    }
}
