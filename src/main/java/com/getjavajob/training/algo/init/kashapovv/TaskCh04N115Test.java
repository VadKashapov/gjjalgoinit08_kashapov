package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh04N115.findYearCalendarName;
import static com.getjavajob.training.algo.init.kashapovv.TaskCh04N115.findYearCalendarNameFrom1984;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 30.10.2016.
 */
public class TaskCh04N115Test {
    public static void main(String[] args) {
        testYearCalendarNameFrom1984();
        testYearCalendarName();
    }

    public static void testYearCalendarNameFrom1984() {
        assertEquals("TaskCh04N115Test.testYearCalendarNameFrom1984.2016", "Red Monkey",
                findYearCalendarNameFrom1984(2016));
        assertEquals("TaskCh04N115Test.testYearCalendarNameFrom1984.1984", "Green Rat",
                findYearCalendarNameFrom1984(1984));
        assertEquals("TaskCh04N115Test.testYearCalendarNameFrom1984.2015", "Green Sheep",
                findYearCalendarNameFrom1984(2015));
        assertEquals("TaskCh04N115Test.testYearCalendarNameFrom1984.2014", "Green Horse",
                findYearCalendarNameFrom1984(2014));
        assertEquals("TaskCh04N115Test.testYearCalendarNameFrom1984.NotValid", "not valid year",
                findYearCalendarNameFrom1984(1944));
    }

    public static void testYearCalendarName() {
        assertEquals("TaskCh04N115Test.testYearCalendarName.2016", "Green Rat", findYearCalendarName(2016));
        assertEquals("TaskCh04N115Test.testYearCalendarName.1", "Green Cow", findYearCalendarName(1));
        assertEquals("TaskCh04N115Test.testYearCalendarName.2", "Red Tiger", findYearCalendarName(2));
        assertEquals("TaskCh04N115Test.testYearCalendarName.NotValid", "not valid year", findYearCalendarName(-3));
    }
}
