package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.util.ConsoleIO.readConsoleDouble;

/**
 * Created by Вадим on 30.10.2016.
 */
public class TaskCh05N010 {
    public static void main(String[] args) {
        double currencyUsdRub = readConsoleDouble();

        double[] valuesUsdRubFirst20 = convertCurrencyFirst20(currencyUsdRub);

        for (int i = 0; i < valuesUsdRubFirst20.length; i++) {
            System.out.println(i + 1 + "  " + valuesUsdRubFirst20[i]);
        }
    }

    public static double[] convertCurrencyFirst20(double currency) {
        double[] valuesFirst20 = new double[20];

        for (int i = 0; i < valuesFirst20.length; i++) {
            valuesFirst20[i] = (i + 1) * currency;
        }
        return valuesFirst20;
    }
}
