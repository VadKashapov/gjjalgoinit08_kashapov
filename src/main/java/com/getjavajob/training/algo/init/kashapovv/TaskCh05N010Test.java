package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh05N010.convertCurrencyFirst20;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 30.10.2016.
 */
public class TaskCh05N010Test {
    public static void main(String[] args) {
        testConvertCurrency();
    }

    public static void testConvertCurrency() {
        double[] valuesUsdRub = new double[]{
                68.0,
                136.0,
                204.0,
                272.0,
                340.0,
                408.0,
                476.0,
                544.0,
                612.0,
                680.0,
                748.0,
                816.0,
                884.0,
                952.0,
                1020.0,
                1088.0,
                1156.0,
                1224.0,
                1292.0,
                1360.0};
        assertEquals("TaskCh05N010Test.testConvertCurrency", valuesUsdRub, convertCurrencyFirst20(68));
    }
}
