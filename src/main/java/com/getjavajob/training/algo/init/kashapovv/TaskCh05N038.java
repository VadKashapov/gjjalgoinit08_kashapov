package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.util.ConsoleIO.isNatural;
import static com.getjavajob.training.algo.util.ConsoleIO.readConsoleInt;

/**
 * Created by Вадим on 31.10.2016.
 */
public class TaskCh05N038 {
    public static void main(String[] args) {
        int stepCount = readConsoleInt();

        double[] distancesToHome = findDistancesToHome(stepCount);

        System.out.println("Husband is " + distancesToHome[0] + " km far from home, and walked totally "
                + distancesToHome[1] + " km");
    }

    /**
     * Method finds distances as a results of alternating & common harmonic series;
     *
     * @param stepCount shows what step we sum harmonic series until;
     * @return return 2-element array with result of alternating & common harmonic series;
     */
    public static double[] findDistancesToHome(int stepCount) {
        if (!isNatural(stepCount)) {
            return new double[]{};
        }
        int signNum = 1;
        double[] distancesToHome = new double[2];

        for (int step = 1; step <= stepCount; step++) {
            distancesToHome[0] += signNum * (1.0 / step);
            distancesToHome[1] += 1.0 / step;
            signNum *= -1;
        }
        return distancesToHome;
    }
}
