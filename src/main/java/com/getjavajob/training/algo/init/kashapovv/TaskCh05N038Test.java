package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh05N038.findDistancesToHome;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 31.10.2016.
 */
public class TaskCh05N038Test {
    public static void main(String[] args) {
        testFindDistancesToHome();
    }

    public static void testFindDistancesToHome() {
        assertEquals("TaskCh05N038Test.testFindDistancesToHome.step100", new double[]{0.688172179310195, 5.187377517639621},
                findDistancesToHome(100));
        assertEquals("TaskCh05N038Test.testFindDistancesToHome.step2", new double[]{0.5, 1.5}, findDistancesToHome(2));
        assertEquals("TaskCh05N038Test.testFindDistancesToHome.notValid", new double[]{}, findDistancesToHome(0));
    }
}
