package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.util.ConsoleIO.fillArrayDouble;

/**
 * Created by Вадим on 31.10.2016.
 */
public class TaskCh05N064 {
    public static void main(String[] args) {
        double[][] districtsParams = new double[2][12];
        fillArrayDouble(districtsParams);
        double popDensity = findPopDensity(districtsParams);

        System.out.println("Population density of region is " + popDensity + " thousands/sq.km");
    }

    public static double findPopDensity(double[][] districtsParams) {
        double popDensity = 0;
        double sumAreas = 0;
        for (int i = 0; i < districtsParams[0].length; i++) {
            popDensity += districtsParams[0][i];
            sumAreas += districtsParams[1][i];
        }
        popDensity /= sumAreas;
        return popDensity;
    }
}
