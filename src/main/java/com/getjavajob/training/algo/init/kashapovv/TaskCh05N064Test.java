package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh05N064.findPopDensity;
import static com.getjavajob.training.algo.util.Assert.assertEquals;


/**
 * Created by Вадим on 31.10.2016.
 */
public class TaskCh05N064Test {
    public static void main(String[] args) {
        testPopDensity();
    }

    public static void testPopDensity() {
        double[][] districtsParams = {
                {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12},
                {10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10}
        };
        assertEquals("TaskCh05N064Test.testPopDensity", 0.65, findPopDensity(districtsParams));
    }
}
