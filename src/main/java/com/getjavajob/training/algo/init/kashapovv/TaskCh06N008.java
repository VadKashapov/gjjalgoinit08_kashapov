package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.util.ConsoleIO.isNatural;
import static com.getjavajob.training.algo.util.ConsoleIO.print;
import static com.getjavajob.training.algo.util.ConsoleIO.readConsoleInt;
import static java.lang.Math.sqrt;

/**
 * Created by Вадим on 31.10.2016.
 */
public class TaskCh06N008 {
    public static void main(String[] args) {
        int num = readConsoleInt();

        int[] squareNums = findSquareNums(num);

        print(squareNums);
    }

    /**
     * Method return sequence of square numbers that not more num;
     */
    public static int[] findSquareNums(int num) {
        if (!isNatural(num)) {
            return new int[]{};
        }
        int size = (int) sqrt(num);
        int[] squareNums = new int[size];
        for (int i = 1; i <= size; i++) {
            squareNums[i - 1] = i * i;
        }
        return squareNums;
    }
}