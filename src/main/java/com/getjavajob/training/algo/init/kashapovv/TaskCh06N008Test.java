package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh06N008.findSquareNums;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 31.10.2016.
 */
public class TaskCh06N008Test {
    public static void main(String[] args) {
        testFindLessSeqNums();
    }

    public static void testFindLessSeqNums() {
        assertEquals("TaskCh06N008Test.testFindLessSeqNums", new int[]{1, 4, 9, 16}, findSquareNums(24));
        assertEquals("TaskCh06N008Test.testFindLessSeqNums", new int[]{1, 4}, findSquareNums(8));
        assertEquals("TaskCh06N008Test.testFindLessSeqNums", new int[]{1}, findSquareNums(1));
        assertEquals("TaskCh06N008Test.testFindLessSeqNums", new int[]{1, 4, 9, 16, 25, 36}, findSquareNums(36));
        assertEquals("TaskCh06N008Test.testFindLessSeqNums", new int[]{}, findSquareNums(-5));
    }
}
