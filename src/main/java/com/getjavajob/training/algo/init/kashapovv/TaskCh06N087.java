package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.util.ConsoleIO.readConsoleInt;
import static com.getjavajob.training.algo.util.ConsoleIO.readConsoleString;

/**
 * Created by Вадим on 31.10.2016.
 */
public class TaskCh06N087 {
    public static void main(String[] args) {
        Game game = new Game();
        game.play();
    }
}

class Game {
    private int teamScore1;
    private int teamScore2;
    private boolean gameFinished;
    private Team team1;
    private Team team2;

    public Game() {
        teamScore1 = 0;
        teamScore2 = 0;
        gameFinished = false;
        team1 = new Team();
        team2 = new Team();
    }

    public void play() {
        System.out.print("Enter team #1: ");
        team1.setTeamName(readConsoleString());
        System.out.print("Enter team #2: ");
        team2.setTeamName(readConsoleString());

        for (; ; ) {
            System.out.print("Enter to score (1 or 2 or 0 to finish game): ");
            int teamNum = readConsoleInt();
            if (teamNum == 0) {
                System.out.println(result());
                gameFinished = true;
                break;
            } else if (teamNum == 1 || teamNum == 2) {
                System.out.print("Enter score (1 or 2 or 3): ");
                int score = readConsoleInt();
                if (score > 0 && score < 4) {
                    if (teamNum == 1) {
                        teamScore1 += score;
                    } else {
                        teamScore2 += score;
                    }
                }
            }
            System.out.println(score());
        }
    }

    public String score() {
        return String.format("%s %d:%d %s", team1.getTeamName(), teamScore1, teamScore2, team2.getTeamName());
    }

    public String result() {
        if (teamScore1 > teamScore2) {
            return String.format("%s - Winner %d:%d %s", team1.getTeamName(), teamScore1, teamScore2, team2.getTeamName());
        } else if (teamScore1 == teamScore2) {
            return String.format("%s %d:%d %s - Stalemate!", team1.getTeamName(), teamScore1, teamScore2, team2.getTeamName());
        } else {
            return String.format("%s %d:%d %s - Winner", team1.getTeamName(), teamScore1, teamScore2, team2.getTeamName());
        }
    }

    public Team getTeam1() {
        return team1;
    }

    public void setTeam1(Team team1) {
        this.team1 = team1;
    }

    public Team getTeam2() {
        return team2;
    }

    public void setTeam2(Team team2) {
        this.team2 = team2;
    }

    public int getTeamScore1() {
        return teamScore1;
    }

    public void setTeamScore1(int teamScore1) {
        this.teamScore1 = teamScore1;
    }

    public int getTeamScore2() {
        return teamScore2;
    }

    public void setTeamScore2(int teamScore2) {
        this.teamScore2 = teamScore2;
    }
}

class Team {
    private String teamName;

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }
}