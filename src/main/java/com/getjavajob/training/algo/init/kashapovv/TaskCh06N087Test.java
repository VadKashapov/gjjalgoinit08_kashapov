package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.util.Assert.assertEquals;


/**
 * Created by Вадим on 31.10.2016.
 */
public class TaskCh06N087Test {
    public static void main(String[] args) {
        testScore();
        testResult();
    }

    public static void testScore() {
        Game game = new Game();
        game.getTeam1().setTeamName("Pirates");
        game.getTeam2().setTeamName("Marines");
        game.setTeamScore1(3);
        game.setTeamScore2(2);
        assertEquals("TaskCh06N087Test.testScore", "Pirates 3:2 Marines", game.score());
    }

    public static void testResult() {
        Game game = new Game();
        game.getTeam1().setTeamName("Pirates");
        game.getTeam2().setTeamName("Marines");
        game.setTeamScore1(3);
        game.setTeamScore2(2);
        assertEquals("TaskCh06N087Test.testResult", "Pirates - Winner 3:2 Marines", game.result());

        game.setTeamScore1(3);
        game.setTeamScore2(3);
        assertEquals("TaskCh06N087Test.testResult", "Pirates 3:3 Marines - Stalemate!", game.result());

        game.setTeamScore1(2);
        game.setTeamScore2(3);
        assertEquals("TaskCh06N087Test.testResult", "Pirates 2:3 Marines - Winner", game.result());
    }
}
