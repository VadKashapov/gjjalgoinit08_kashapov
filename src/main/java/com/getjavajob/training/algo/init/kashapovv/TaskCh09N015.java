package com.getjavajob.training.algo.init.kashapovv;


import java.util.Scanner;

/**
 * Created by Вадим on 01.11.2016.
 */
public class TaskCh09N015 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String word = in.next();
        int index = in.nextInt();

        System.out.println("k-char is " + charAt(index, word));

    }

    public static char charAt(int index, String word) {
        return word.charAt(index + 1);
    }
}
