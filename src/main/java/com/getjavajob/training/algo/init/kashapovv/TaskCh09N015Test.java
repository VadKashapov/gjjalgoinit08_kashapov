package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh09N015.charAt;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 01.11.2016.
 */
public class TaskCh09N015Test {
    public static void main(String[] args) {
        testCharAt();
    }

    public static void testCharAt() {
        assertEquals("TaskCh06N087Test.testCharAt", 'a', charAt(5, "Alabama"));
    }
}
