package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

/**
 * Created by Вадим on 01.11.2016.
 */
public class TaskCh09N017 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String word = in.next();

        boolean equal = isFirstLastCharEquals(word);

        System.out.print("First & last char equal? " + equal);
    }

    public static boolean isFirstLastCharEquals(String word) {
        String firstChar = String.valueOf(word.charAt(0)).toLowerCase();
        String lastChar = String.valueOf(word.charAt(word.length() - 1)).toLowerCase();
        return firstChar.equals(lastChar);
    }
}
