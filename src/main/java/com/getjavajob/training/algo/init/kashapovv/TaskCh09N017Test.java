package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh09N017.isFirstLastCharEquals;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 01.11.2016.
 */
public class TaskCh09N017Test {
    public static void main(String[] args) {
        testFirstLastCharEqual();
    }

    public static void testFirstLastCharEqual() {
        assertEquals("TaskCh09N017Test.testFirstLastCharEqual", true, isFirstLastCharEquals("DDQd"));
        assertEquals("TaskCh09N017Test.testFirstLastCharEqual", false, isFirstLastCharEquals("DDQq"));
    }
}
