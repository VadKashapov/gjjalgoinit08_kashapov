package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.util.ConsoleIO.isEven;
import static com.getjavajob.training.algo.util.ConsoleIO.readConsoleString;

/**
 * Created by Вадим on 01.11.2016.
 */
public class TaskCh09N022 {
    public static void main(String[] args) {
        String word = readConsoleString();

        String halfWord = findFirstHalfWord(word);

        System.out.println("First half of word is " + halfWord);
    }

    public static String findFirstHalfWord(String word) {
        if (isEven(word.length())) {
            return word.substring(0, word.length() / 2);
        } else {
            return "-1. word not even";
        }
    }
}
