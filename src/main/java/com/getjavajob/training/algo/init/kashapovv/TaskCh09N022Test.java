package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh09N022.findFirstHalfWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 01.11.2016.
 */
public class TaskCh09N022Test {
    public static void main(String[] args) {
        testHalfWord();
    }

    public static void testHalfWord() {
        assertEquals("TaskCh09N022Test.testHalfWord", "Apple", findFirstHalfWord("Appleseede"));
        assertEquals("TaskCh09N022Test.testHalfWord", "-1. word not even", findFirstHalfWord("Appleseed"));
    }
}
