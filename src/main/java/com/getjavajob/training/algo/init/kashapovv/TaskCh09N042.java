package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.util.ConsoleIO.readConsoleString;

/**
 * Created by Вадим on 01.11.2016.
 */
public class TaskCh09N042 {
    public static void main(String[] args) {
        String word = readConsoleString();

        String reversedWord = reverseWord(word);

        System.out.println(reversedWord);
    }

    public static String reverseWord(String word) {
        StringBuilder build = new StringBuilder(word.length());
        return build.append(word).reverse().toString();
    }
}
