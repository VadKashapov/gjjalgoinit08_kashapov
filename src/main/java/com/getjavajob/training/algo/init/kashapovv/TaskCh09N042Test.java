package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh09N042.reverseWord;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 08.11.2016.
 */
public class TaskCh09N042Test {
    public static void main(String[] args) {
        testReverse();
    }

    public static void testReverse() {
        assertEquals("TaskCh09N042Test.testReverse", "deeselppA", reverseWord("Appleseed"));
    }
}
