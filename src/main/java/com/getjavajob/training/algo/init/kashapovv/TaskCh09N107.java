package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.util.ConsoleIO.readConsoleString;

/**
 * Created by Вадим on 02.11.2016.
 */
public class TaskCh09N107 {
    public static void main(String[] args) {
        String word = readConsoleString();

        String swapWord = swapFirstALastO(word);

        System.out.println(swapWord);
    }

    /**
     * Method swap first 'a' and last 'o' symbols in word
     **/
    public static String swapFirstALastO(String word) {
        boolean foundA = false;
        boolean foundO = false;
        int i = 0;
        int j = word.length() - 1;
        StringBuilder build = new StringBuilder(word.length());
        build.append(word);
        for (; i < word.length(); i++) {
            if (word.charAt(i) == 'a') {
                foundA = true;
                break;
            }
        }
        for (; j >= 0; j--) {
            if (word.charAt(j) == 'o') {
                foundO = true;
                break;
            }
        }
        if (foundA && foundO) {
            build.setCharAt(i, 'o');
            build.setCharAt(j, 'a');
        }
        return build.toString();
    }
}