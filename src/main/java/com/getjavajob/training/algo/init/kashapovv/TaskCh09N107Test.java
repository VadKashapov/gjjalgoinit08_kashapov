package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh09N107.swapFirstALastO;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 08.11.2016.
 */
public class TaskCh09N107Test {
    public static void main(String[] args) {
        testSwapFirstLast();
    }

    public static void testSwapFirstLast() {
        assertEquals("TaskCh09N107Test.testSwapFirstLast", "pondoraTomorraw", swapFirstALastO("pandoraTomorrow"));
        assertEquals("TaskCh09N107Test.testSwapFirstLast", "pomodoro", swapFirstALastO("pomodoro"));
    }
}
