package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.util.ConsoleIO.readConsoleString;

/**
 * Created by Вадим on 02.11.2016.
 */
public class TaskCh09N166 {
    public static void main(String[] args) {
        String sentence = readConsoleString();

        String newSentence = swapFirstLastWords(sentence);

        System.out.println(newSentence);
    }

    public static String swapFirstLastWords(String sentence) {
        String[] words = sentence.split("\\s");
        StringBuilder buildString = new StringBuilder(sentence.length());
        buildString.append(words[words.length - 1]);
        if (words.length > 1) {
            for (int i = 1; i < words.length - 1; i++) {
                buildString.append(' ').append(words[i]);
            }
            buildString.append(' ').append(words[0]);
        }
        return buildString.toString();
    }
}
