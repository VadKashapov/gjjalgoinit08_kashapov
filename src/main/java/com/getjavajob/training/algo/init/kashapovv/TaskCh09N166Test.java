package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh09N166.swapFirstLastWords;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 08.11.2016.
 */
public class TaskCh09N166Test {
    public static void main(String[] args) {
        testThreeWords();
        testTwoWords();
        testOneWord();
    }

    public static void testThreeWords() {
        assertEquals("TaskCh09N166Test.testThreeWords", "Pandora coming tomorrow",
                swapFirstLastWords("tomorrow coming Pandora"));
    }

    public static void testTwoWords() {
        assertEquals("TaskCh09N166Test.testTwoWords", "Pandora tomorrow",
                swapFirstLastWords("tomorrow Pandora"));
    }

    public static void testOneWord() {
        assertEquals("TaskCh09N166Test.testOneWord", "Pandora", swapFirstLastWords("Pandora"));
    }
}
