package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.util.ConsoleIO.readConsoleString;

/**
 * Created by Вадим on 03.11.2016.
 */
public class TaskCh09N185 {
    public static void main(String[] args) {
        String sentence = readConsoleString();

        String bracketCheckResult = checkBrackets(sentence);

        System.out.println(bracketCheckResult);
    }

    /**
     * Method checks the integrity of bracket series in arithmetic sentence;
     *
     * @param sentence contain words separated by whitespaces;
     * @return return string "yes", if sentence has bracket integrity,
     * or string with left brackets count or string with last bracket index,
     * if sentence has left or right bracket violation respectively;
     */
    public static String checkBrackets(String sentence) {
        int bracket = 0;
        int leftBracket = 0;
        int indexBracket = 0;

        for (int i = 0; i < sentence.length(); i++) {
            if (sentence.charAt(i) == '(') {
                bracket++;
                leftBracket++;
            } else if (sentence.charAt(i) == ')') {
                bracket--;
                indexBracket = i;
            }
        }
        if (bracket == 0) {
            return "yes";
        } else if (bracket > 0) {
            return "no, left brackets count is " + leftBracket;
        } else {
            return "no, index last right bracket is " + indexBracket;
        }
    }
}
