package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh09N185.checkBrackets;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 08.11.2016.
 */
public class TaskCh09N185Test {
    public static void main(String[] args) {
        testCheckBrackets();
    }

    public static void testCheckBrackets() {
        assertEquals("TaskCh09N185Test.testCheckNoViolation", "yes", checkBrackets("(()())"));
        assertEquals("TaskCh09N185Test.testCheckLeftViolation", "no, left brackets count is 4",
                checkBrackets("(((()))"));
        assertEquals("TaskCh09N185Test.testCheckRightViolation", "no, index last right bracket is 6",
                checkBrackets("(()))))"));
    }
}
