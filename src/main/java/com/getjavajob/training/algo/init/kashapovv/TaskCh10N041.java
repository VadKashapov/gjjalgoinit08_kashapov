package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

/**
 * Created by Вадим on 03.11.2016.
 */
public class TaskCh10N041 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();

        int factorialNum = getFactorial(num);

        System.out.println(factorialNum);
    }

    public static int getFactorial(int num) {
        if (num < 0) {
            return -1;
        }
        return num != 0 ? num * getFactorial(num - 1) : 1;
    }
}
