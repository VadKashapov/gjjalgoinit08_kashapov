package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh10N041.getFactorial;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 08.11.2016.
 */
public class TaskCh10N041Test {
    public static void main(String[] args) {
        testNumInFact();
    }

    public static void testNumInFact() {
        assertEquals("TaskCh10N041Test.testNumInFact.5", 120, getFactorial(5));
        assertEquals("TaskCh10N041Test.testNumInFact.0", 1, getFactorial(0));
        assertEquals("TaskCh10N041Test.testNumInFact.notValid", -1, getFactorial(-5));
    }
}
