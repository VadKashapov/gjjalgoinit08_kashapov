package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

/**
 * Created by Вадим on 03.11.2016.
 */
public class TaskCh10N042 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        double base = in.nextDouble();
        int power = in.nextInt();

        double result = getPow(base, power);

        System.out.println(result);
    }

    public static double getPow(double base, int power) {
        if (power < 0) {
            return -1;
        }
        return power != 0 ? base * getPow(base, power - 1) : 1;
    }
}
