package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh10N042.getPow;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 09.11.2016.
 */
public class TaskCh10N042Test {
    public static void main(String[] args) {
        testGetPow();
    }

    public static void testGetPow() {
        assertEquals("TaskCh10N042Test.testGetPow.5^2", 25, getPow(5, 2));
        assertEquals("TaskCh10N042Test.testGetPow.5^0", 1, getPow(5, 0));
        assertEquals("TaskCh10N042Test.testGetPow.NotValid", -1, getPow(5, -5));
    }
}
