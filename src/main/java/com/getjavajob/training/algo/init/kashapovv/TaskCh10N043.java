package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

/**
 * Created by Вадим on 03.11.2016.
 */
public class TaskCh10N043 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();
        int digitSum = findDigitSum(num);
        int digitCount = findDigitCount(num);

        System.out.println(digitSum);
        System.out.println(digitCount);
    }

    public static int findDigitSum(int num) {
        if (num < 0) {
            return -1;
        }
        return num != 0 ? num % 10 + findDigitSum(num / 10) : 0;
    }

    public static int findDigitCount(int num) {
        if (num < 0) {
            return -1;
        }
        return num != 0 ? findDigitCount(num / 10) + 1 : 0;
    }
}
