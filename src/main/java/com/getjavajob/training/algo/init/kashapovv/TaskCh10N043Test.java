package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh10N043.findDigitCount;
import static com.getjavajob.training.algo.init.kashapovv.TaskCh10N043.findDigitSum;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 09.11.2016.
 */
public class TaskCh10N043Test {
    public static void main(String[] args) {
        testFindDigitCount();
        testFindDigitSum();
    }

    public static void testFindDigitCount() {
        assertEquals("TaskCh10N043Test.testFindDigitCount", 4, findDigitCount(1231));
        assertEquals("TaskCh10N043Test.testFindDigitCount", -1, findDigitCount(-1));
    }

    public static void testFindDigitSum() {
        assertEquals("TaskCh10N043Test.testFindDigitSum", 7, findDigitSum(1231));
        assertEquals("TaskCh10N043Test.testFindDigitSum", -1, findDigitSum(-500));
    }
}
