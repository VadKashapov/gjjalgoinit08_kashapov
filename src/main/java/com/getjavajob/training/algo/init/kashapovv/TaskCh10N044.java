package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

/**
 * Created by Вадим on 03.11.2016.
 */
public class TaskCh10N044 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();
        int digitRoot = findDigitRoot(num);

        System.out.println(digitRoot);
    }

    public static int findDigitRoot(int num) {
        if (num < 0) {
            return -1;
        }
        return num / 10 != 0 ? findDigitRoot(num % 10 + findDigitRoot(num / 10)) : num % 10;
    }
}
