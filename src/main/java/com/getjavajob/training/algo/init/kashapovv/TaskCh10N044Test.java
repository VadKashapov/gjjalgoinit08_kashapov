package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh10N044.findDigitRoot;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 09.11.2016.
 */
public class TaskCh10N044Test {
    public static void main(String[] args) {
        testFindDigitRoot();
    }

    public static void testFindDigitRoot() {
        assertEquals("TaskCh10N044Test.testFindDigitRoot", 7, findDigitRoot(1861));
        assertEquals("TaskCh10N044Test.testFindDigitRoot", -1, findDigitRoot(-500));
    }
}
