package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

/**
 * Created by Вадим on 03.11.2016.
 */
public class TaskCh10N045 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int first = in.nextInt();
        int diff = in.nextInt();
        int index = in.nextInt();

        int num = findNumProg(first, diff, index);
        int sum = findSumProg(first, diff, index);

        System.out.println(num);
        System.out.println(sum);
    }

    public static int findNumProg(int first, int diff, int index) {
        return index != 0 ? findNumProg(first + diff, diff, index - 1) : first;
    }

    public static int findSumProg(int first, int diff, int index) {
        return index != 0 ? first + findSumProg(first + diff, diff, index - 1) : first;
    }
}
