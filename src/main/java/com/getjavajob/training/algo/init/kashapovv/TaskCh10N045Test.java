package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh10N045.findNumProg;
import static com.getjavajob.training.algo.init.kashapovv.TaskCh10N045.findSumProg;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 09.11.2016.
 */
public class TaskCh10N045Test {
    public static void main(String[] args) {
        testFindNumProg();
        testFindSumProg();
    }

    public static void testFindNumProg() {
        assertEquals("TaskCh10N045Test.testFindNumProg", 5, findNumProg(1, 2, 2));
        assertEquals("TaskCh10N045Test.testFindNumProg", 1, findNumProg(1, 2, 0));
    }

    public static void testFindSumProg() {
        assertEquals("TaskCh10N045Test.testFindSumProg", 9, findSumProg(1, 2, 2));
        assertEquals("TaskCh10N045Test.testFindSumProg", 1, findSumProg(1, 2, 0));
    }
}
