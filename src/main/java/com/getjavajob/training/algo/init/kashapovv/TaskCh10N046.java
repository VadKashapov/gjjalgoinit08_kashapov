package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

/**
 * Created by Вадим on 03.11.2016.
 */
public class TaskCh10N046 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int first = in.nextInt();
        int diff = in.nextInt();
        int index = in.nextInt();

        int num = findNumGProg(first, diff, index);
        int sum = findSumGProg(first, diff, index);

        System.out.println(num);
        System.out.println(sum);
    }

    public static int findNumGProg(int first, int diff, int index) {
        return index > 1 ? findNumGProg(first * diff, diff, index - 1) : first;
    }

    public static int findSumGProg(int first, int diff, int index) {
        return index > 1 ? first + findSumGProg(first * diff, diff, index - 1) : first;
    }
}
