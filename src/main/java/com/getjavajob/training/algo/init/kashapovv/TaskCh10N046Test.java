package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh10N046.findNumGProg;
import static com.getjavajob.training.algo.init.kashapovv.TaskCh10N046.findSumGProg;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 09.11.2016.
 */
public class TaskCh10N046Test {
    public static void main(String[] args) {
        testFindNumGProg();
        testFindSumGProg();
    }

    public static void testFindNumGProg() {
        assertEquals("TaskCh10N046Test.testFindNumGProg", 8, findNumGProg(2, 2, 3));
        assertEquals("TaskCh10N046Test.testFindNumGProg", 2, findNumGProg(2, 2, 0));
    }

    public static void testFindSumGProg() {
        assertEquals("TaskCh10N046Test.testFindSumGProg", 7, findSumGProg(1, 2, 3));
        assertEquals("TaskCh10N046Test.testFindSumGProg", 1, findSumGProg(1, 2, 0));
    }
}
