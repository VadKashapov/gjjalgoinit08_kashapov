package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

/**
 * Created by Вадим on 03.11.2016.
 */
public class TaskCh10N047 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int index = in.nextInt();

        int num = findNumFibb(index);

        System.out.println(num);
    }

    public static int findNumFibb(int index) {
        return index < 2 ? index : findNumFibb(index - 1) + findNumFibb(index - 2);
    }
}
