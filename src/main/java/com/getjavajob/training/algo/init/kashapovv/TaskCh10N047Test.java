package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh10N047.findNumFibb;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 09.11.2016.
 */
public class TaskCh10N047Test {
    public static void main(String[] args) {
        testFindNumFibb();
    }

    public static void testFindNumFibb() {

        assertEquals("TaskCh10N047Test.testFindNumFibb", 21, findNumFibb(8));
        assertEquals("TaskCh10N047Test.testFindNumFibb", 1, findNumFibb(1));
    }
}
