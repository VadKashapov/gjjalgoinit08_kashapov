package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.util.ConsoleIO.fillArrayInt;
import static com.getjavajob.training.algo.util.ConsoleIO.readConsoleInt;

/**
 * Created by Вадим on 03.11.2016.
 */
public class TaskCh10N048 {
    public static void main(String[] args) {
        int size = readConsoleInt();
        int[] numbers = new int[size];
        fillArrayInt(numbers);

        int maxElement = findMaxElement(numbers, size);

        System.out.println(maxElement);
    }

    public static int findMaxElement(int[] numbers) {
        return findMaxElement(numbers, numbers.length);
    }

    public static int findMaxElement(int[] numbers, int index) {
        if (index - 1 > 0) {
            if (numbers[index - 1] > numbers[index - 2]) {
                numbers[index - 2] = numbers[index - 1];
            }
            findMaxElement(numbers, index - 1);
        }
        return numbers[0];
    }
}
