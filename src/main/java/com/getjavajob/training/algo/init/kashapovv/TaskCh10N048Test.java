package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh10N048.findMaxElement;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 09.11.2016.
 */
public class TaskCh10N048Test {
    public static void main(String[] args) {
        testFindMaxElement();
    }

    public static void testFindMaxElement() {
        int[] numbers = {3, 2, 15, -3, 5, 0, 5};
        assertEquals("TaskCh10N048Test.testFindMaxElement", 15, findMaxElement(numbers));
    }
}
