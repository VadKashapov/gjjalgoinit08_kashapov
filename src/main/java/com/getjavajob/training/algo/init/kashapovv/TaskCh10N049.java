package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

import static com.getjavajob.training.algo.util.ConsoleIO.fillArrayInt;

/**
 * Created by Вадим on 03.11.2016.
 */
public class TaskCh10N049 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int size = in.nextInt();
        int[] numbers = new int[size];
        fillArrayInt(numbers);

        int indexMax = findIndexMax(numbers);

        System.out.println(indexMax);
    }

    public static int findIndexMax(int[] numbers) {
        return findIndexMax(numbers, numbers.length, 0);
    }

    public static int findIndexMax(int[] numbers, int size, int index) {
        if (size - 1 > 0) {
            if (numbers[index] < numbers[size - 2]) {
                return findIndexMax(numbers, size - 1, size - 2);
            } else {
                return findIndexMax(numbers, size - 1, index);
            }
        }
        return index;
    }
}
