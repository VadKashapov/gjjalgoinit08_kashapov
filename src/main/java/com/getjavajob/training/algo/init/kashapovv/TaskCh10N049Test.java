package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh10N049.findIndexMax;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 10.11.2016.
 */
public class TaskCh10N049Test {
    public static void main(String[] args) {
        testFindIndexMax();
    }

    public static void testFindIndexMax() {
        int[] numbers = {3, 2, 15, -3, 5, 0, 5};
        assertEquals("TaskCh10N049Test.testFindIndexMax", 2, findIndexMax(numbers));
    }
}
