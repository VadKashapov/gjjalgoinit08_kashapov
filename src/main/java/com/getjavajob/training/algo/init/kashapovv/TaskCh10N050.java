package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

/**
 * Created by Вадим on 03.11.2016.
 */
public class TaskCh10N050 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();
        int result = findAckerman(n, m);
        System.out.print(result);
    }

    /**
     * Method return Ackerman function value
     **/
    public static int findAckerman(int n, int m) {
        if (n < 0 || m < 0) {
            return -1;
        }
        if (n == 0) {
            return m + 1;
        }
        if (m == 0) {
            return findAckerman(n - 1, 1);
        }
        return n > 0 && m > 0 ? findAckerman(n - 1, findAckerman(n, m - 1)) : 0;
    }
}
