package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh10N050.findAckerman;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 10.11.2016.
 */
public class TaskCh10N050Test {
    public static void main(String[] args) {
        testFuncAckerman();
    }

    public static void testFuncAckerman() {
        assertEquals("TaskCh10N050Test.testFuncAckerman.1,3", 5, findAckerman(1, 3));
        assertEquals("TaskCh10N050Test.testFuncAckerman.0,1", 2, findAckerman(0, 1));
        assertEquals("TaskCh10N050Test.testFuncAckerman.1,0", 2, findAckerman(1, 0));
        assertEquals("TaskCh10N050Test.testFuncAckerman.0,0", 1, findAckerman(0, 0));
        assertEquals("TaskCh10N050Test.testFuncAckerman.-1,3", -1, findAckerman(-1, 3));
    }
}
