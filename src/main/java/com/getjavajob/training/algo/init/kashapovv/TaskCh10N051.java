package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

/**
 * Created by Вадим on 03.11.2016.
 */
public class TaskCh10N051 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        runProcedure1(n);
        System.out.println();
        runProcedure2(n);
        System.out.println();
        runProcedure3(n);
    }

    public static void runProcedure1(int n) {
        if (n > 0) {
            System.out.print(n);
            runProcedure1(n - 1);
        }
    }

    public static void runProcedure2(int n) {
        if (n > 0) {
            runProcedure2(n - 1);
            System.out.print(n);
        }
    }

    public static void runProcedure3(int n) {
        if (n > 0) {
            System.out.print(n);
            runProcedure3(n - 1);
            System.out.print(n);
        }
    }
}
