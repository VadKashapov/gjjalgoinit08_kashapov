package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

/**
 * Created by Вадим on 04.11.2016.
 */
public class TaskCh10N052 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int number = in.nextInt();

        int reversedNum = reverseNum(number);

        System.out.println(reversedNum);
    }

    public static int reverseNum(int number) {
        return number > 0 ? reverseNum(number, 0) : -1;
    }

    public static int reverseNum(int number, int count) {
        if (number != 0) {
            return reverseNum(number / 10, count * 10 + number % 10);
        } else {
            return count;
        }
    }
}