package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh10N052.reverseNum;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 15.11.2016.
 */
public class TaskCh10N052Test {
    public static void main(String[] args) {
        testReverseNum();
    }

    public static void testReverseNum() {
        assertEquals("TaskCh10N052Test.testChangeNotation", 6541, reverseNum(1456));
        assertEquals("TaskCh10N052Test.testChangeNotation", 51, reverseNum(1500));
        assertEquals("TaskCh10N052Test.testChangeNotation", -1, reverseNum(0));
        assertEquals("TaskCh10N052Test.testChangeNotation", -1, reverseNum(-20));
    }
}
