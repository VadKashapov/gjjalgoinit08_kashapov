package com.getjavajob.training.algo.init.kashapovv;

import java.util.Arrays;

import static com.getjavajob.training.algo.util.ConsoleIO.fillArrayInt;
import static com.getjavajob.training.algo.util.ConsoleIO.readConsoleInt;

/**
 * Created by Вадим on 04.11.2016.
 */
public class TaskCh10N053 {
    private static int[] tempNumbers;

    public static void main(String[] args) {
        int size = readConsoleInt();
        System.out.print("input array values with 0 at end");
        int[] numbers = new int[size];
        fillArrayInt(numbers);

        int[] reversedNumbers = reverseArray(numbers);

        System.out.println(Arrays.toString(reversedNumbers));
    }

    public static int[] reverseArray(int[] numbers) {
        return reverseArray(numbers, 0);
    }

    public static int[] reverseArray(int[] numbers, int index) {
        tempNumbers = new int[numbers.length - 1];
        if (numbers[index] != 0) {
            reverseArray(numbers, index + 1);
            tempNumbers[numbers.length - index - 2] = numbers[index];
        }
        return tempNumbers;
    }
}
