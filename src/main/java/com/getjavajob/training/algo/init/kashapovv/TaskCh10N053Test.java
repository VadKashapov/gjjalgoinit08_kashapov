package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh10N053.reverseArray;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 15.11.2016.
 */
public class TaskCh10N053Test {
    public static void main(String[] args) {
        testReverseNum();
    }

    public static void testReverseNum() {
        assertEquals("TaskCh10N053Test.testReverseNum", new int[]{5, 4, 3, 2, 1},
                reverseArray(new int[]{1, 2, 3, 4, 5, 0}));
        assertEquals("TaskCh10N053Test.testReverseNum", new int[]{}, reverseArray(new int[]{0}));
    }
}
