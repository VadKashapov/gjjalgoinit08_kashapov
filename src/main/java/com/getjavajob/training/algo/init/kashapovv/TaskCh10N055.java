package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

/**
 * Created by Вадим on 04.11.2016.
 */
public class TaskCh10N055 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int sysNum = in.nextInt();
        int origNum = in.nextInt();
        int result = changeNotation(sysNum, origNum);
        System.out.print(result);
    }


    public static int changeNotation(int sysNum, int origNum) {
        if (sysNum < 2 || sysNum > 16 || origNum < 1) {
            return -1;
        }
        if (origNum >= sysNum) {
            return changeNotation(sysNum, origNum / sysNum) * 10 + origNum % sysNum;
        } else {
            return origNum;
        }
    }
}
