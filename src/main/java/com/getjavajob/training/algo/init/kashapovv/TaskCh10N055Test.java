package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh10N055.changeNotation;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 10.11.2016.
 */
public class TaskCh10N055Test {
    public static void main(String[] args) {
        testChangeNotation();
    }

    public static void testChangeNotation() {
        assertEquals("TaskCh10N055Test.testChangeNotation", 154, changeNotation(16, 244));
        assertEquals("TaskCh10N055Test.testChangeNotation", 364, changeNotation(8, 244));
        assertEquals("TaskCh10N055Test.testChangeNotation", 210, changeNotation(11, 252));
        assertEquals("TaskCh10N055Test.testChangeNotation", 184, changeNotation(12, 244));
        assertEquals("TaskCh10N055Test.testChangeNotation", 160, changeNotation(13, 244));
        assertEquals("TaskCh10N055Test.testChangeNotation", -1, changeNotation(26, 244));
    }
}
