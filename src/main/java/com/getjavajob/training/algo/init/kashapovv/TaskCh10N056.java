package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

import static com.getjavajob.training.algo.util.ConsoleIO.isNatural;
import static java.lang.Math.sqrt;

/**
 * Created by Вадим on 04.11.2016.
 */
public class TaskCh10N056 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = in.nextInt();

        boolean result = isPrime(num);

        System.out.print(result);
    }

    public static boolean isPrime(int num) {
        if (isNatural(num)) {
            return isPrime(num, 2);
        } else {
            System.out.println("Error: not valid number");
            return false;
        }
    }

    public static boolean isPrime(int num, int div) {
        return div > sqrt(num) || num % div != 0 && isPrime(num, div + 1);
    }
}