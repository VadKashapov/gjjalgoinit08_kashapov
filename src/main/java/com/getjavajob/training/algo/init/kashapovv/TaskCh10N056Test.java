package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh10N056.isPrime;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 10.11.2016.
 */
public class TaskCh10N056Test {
    public static void main(String[] args) {
        testIsPrime();
    }

    public static void testIsPrime() {
        assertEquals("TaskCh10N055Test.testIsPrime", true, isPrime(139));
        assertEquals("TaskCh10N055Test.testIsPrime", false, isPrime(189));
        assertEquals("TaskCh10N055Test.testIsPrime", false, isPrime(-6));
    }
}
