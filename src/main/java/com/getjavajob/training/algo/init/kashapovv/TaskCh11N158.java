package com.getjavajob.training.algo.init.kashapovv;

import java.util.Arrays;
import java.util.Scanner;

import static com.getjavajob.training.algo.util.ConsoleIO.fillArrayInt;

/**
 * Created by Вадим on 05.11.2016.
 */
public class TaskCh11N158 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int size = in.nextInt();
        int[] numbers = new int[size];
        fillArrayInt(numbers);

        int[] numbersWithoutDuplicates = delDuplicate(numbers);

        System.out.println(Arrays.toString(numbersWithoutDuplicates));
    }

    public static int[] delDuplicate(int[] numbers) {
        for (int i = 0, delPos = numbers.length; i < delPos; i++) {
            for (int j = 0; j < delPos; j++) {
                if (i != j && numbers[i] == numbers[j]) {
                    delPos--;
                    for (int t = j; t < numbers.length - 1; t++) {
                        numbers[t] = numbers[t + 1];
                        numbers[t + 1] = 0;
                    }
                }
            }
        }
        return numbers;
    }
}
