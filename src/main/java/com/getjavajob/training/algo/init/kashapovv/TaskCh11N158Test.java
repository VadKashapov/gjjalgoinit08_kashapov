package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh11N158.delDuplicate;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 09.11.2016.
 */
public class TaskCh11N158Test {
    public static void main(String[] args) {
        testDelDuplicate();
    }

    public static void testDelDuplicate() {
        int[] numbers = {1, 0, 5, 3, 0, 2, 7, 3};
        int[] numbersWithoutDuplicates = {1, 0, 5, 3, 2, 7, 0, 0};
        assertEquals("TaskCh11N158Test.testDelDuplicate", numbersWithoutDuplicates, delDuplicate(numbers));
    }
}
