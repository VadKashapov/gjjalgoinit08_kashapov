package com.getjavajob.training.algo.init.kashapovv;

import java.util.Arrays;
import java.util.Scanner;

import static com.getjavajob.training.algo.util.ConsoleIO.fillArrayInt;

/**
 * Created by Вадим on 05.11.2016.
 */
public class TaskCh11N245 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int size = in.nextInt();
        int[] numbers = new int[size];
        fillArrayInt(numbers);

        int[] sortedNumbers = signSort(numbers);

        System.out.println(Arrays.toString(sortedNumbers));
    }

    public static int[] signSort(int[] numbers) {
        int[] copyNumbers = new int[numbers.length];
        int t = copyNumbers.length - 1;
        for (int i = 0, j = 0; i < numbers.length; i++) {
            if (numbers[i] < 0) {
                copyNumbers[j++] = numbers[i];
            } else {
                copyNumbers[t--] = numbers[i];
            }
        }
        return copyNumbers;
    }
}
