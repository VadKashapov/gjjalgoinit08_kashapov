package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh11N245.signSort;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 09.11.2016.
 */
public class TaskCh11N245Test {
    public static void main(String[] args) {
        testSortCopy();
    }

    public static void testSortCopy() {
        int[] numbers = {1, -2, -3, 4, 5, -6, 7, -8};
        int[] sortedNumbers = {-2, -3, -6, -8, 7, 5, 4, 1};
        assertEquals("TaskCh11N245Test.testSortCopy", sortedNumbers, signSort(numbers));
    }
}
