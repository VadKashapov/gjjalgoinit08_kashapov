package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

import static com.getjavajob.training.algo.util.ConsoleIO.isEven;
import static com.getjavajob.training.algo.util.ConsoleIO.print;

/**
 * Created by Вадим on 05.11.2016.
 */
public class TaskCh12N023 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int size = in.nextInt();
        int[][] numbers = new int[size][size];

        numbers = fillArrX(numbers);
        print(numbers);

        numbers = new int[size][size];
        numbers = fillArrStar(numbers);
        print(numbers);

        numbers = new int[size][size];
        numbers = fillArrHourGlass(numbers);
        print(numbers);
    }

    public static int[][] fillArrX(int[][] numbers) {
        if (isEven(numbers.length)) {
            return new int[][]{};
        }
        for (int i = 0; i < numbers.length; i++) {
            numbers[i][i] = 1;
            numbers[numbers.length - i - 1][i] = 1;
        }
        return numbers;
    }

    public static int[][] fillArrStar(int[][] numbers) {
        if (isEven(numbers.length)) {
            return new int[][]{};
        }
        for (int i = 0; i < numbers.length; i++) {
            numbers[i][i] = 1;
            numbers[numbers.length - i - 1][i] = 1;
            numbers[numbers.length / 2][i] = 1;
            numbers[i][numbers.length / 2] = 1;
        }
        return numbers;
    }

    public static int[][] fillArrHourGlass(int[][] numbers) {
        if (isEven(numbers.length)) {
            return new int[][]{};
        }
        for (int i = 0; i < numbers.length; i++) {
            for (int j = i; j < numbers.length - i; j++) {
                numbers[i][j] = 1;
                numbers[numbers.length - i - 1][j] = 1;
            }
        }
        return numbers;
    }
}
