package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh12N023.*;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 09.11.2016.
 */
public class TaskCh12N023Test {
    public static void main(String[] args) {
        testFillArrX();
        testFillArrStar();
        testFillArrHourGlass();
    }

    public static void testFillArrX() {
        int[][] numbers = new int[7][7];
        int[][] resultNumbers = {
                {1, 0, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 1, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0, 1}};

        assertEquals("TaskCh12N023Test.testFillArrX", resultNumbers, fillArrX(numbers));
    }

    public static void testFillArrStar() {
        int[][] n = new int[7][7];
        int[][] res = new int[][]{
                {1, 0, 0, 1, 0, 0, 1},
                {0, 1, 0, 1, 0, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {1, 1, 1, 1, 1, 1, 1},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 0, 1, 0, 1, 0},
                {1, 0, 0, 1, 0, 0, 1}};

        assertEquals("TaskCh12N023Test.testFillArrStar", res, fillArrStar(n));
    }

    public static void testFillArrHourGlass() {
        int[][] n = new int[7][7];
        int[][] res = new int[][]{
                {1, 1, 1, 1, 1, 1, 1},
                {0, 1, 1, 1, 1, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 1}};

        assertEquals("TaskCh12N023Test.testFillArrHourGlass", res, fillArrHourGlass(n));
    }
}
