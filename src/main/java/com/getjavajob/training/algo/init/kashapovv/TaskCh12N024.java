package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

import static com.getjavajob.training.algo.util.ConsoleIO.isEven;
import static com.getjavajob.training.algo.util.ConsoleIO.print;

/**
 * Created by Вадим on 05.11.2016.
 */
public class TaskCh12N024 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int size = in.nextInt();
        int[][] numbers = new int[size][size];

        numbers = fillArrSum(numbers);
        print(numbers);

        numbers = new int[size][size];
        numbers = fillArrShift(numbers);
        print(numbers);
    }

    /**
     * Method fill square array by sum up left and upper elements on empty position;
     */
    public static int[][] fillArrSum(int[][] numbers) {
        if (!isEven(numbers.length) || !isEven(numbers[0].length)) {
            return new int[][]{};
        }
        for (int j = 0; j < numbers.length; j++) {
            numbers[0][j] = 1;
            numbers[j][0] = 1;
        }
        for (int i = 1; i < numbers.length; i++) {
            for (int j = 1; j < numbers.length; j++) {
                numbers[i][j] = numbers[i][j - 1] + numbers[i - 1][j];
            }
        }
        return numbers;
    }

    /**
     * Method fill square array by copy previous column with right shift elements;
     */
    public static int[][] fillArrShift(int[][] numbers) {
        if (!isEven(numbers.length) || !isEven(numbers[0].length)) {
            return new int[][]{};
        }
        for (int i = 0; i < numbers.length; i++) {
            for (int j = 0; j < numbers[0].length; j++) {
                if (i + j > numbers.length - 1) {
                    numbers[i][j] = j - numbers.length + i + 1;
                } else {
                    numbers[i][j] = i + j + 1;
                }
            }
        }
        return numbers;
    }
}
