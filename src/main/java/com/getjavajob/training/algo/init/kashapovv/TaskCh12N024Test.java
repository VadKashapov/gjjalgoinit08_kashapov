package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh12N024.fillArrShift;
import static com.getjavajob.training.algo.init.kashapovv.TaskCh12N024.fillArrSum;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 09.11.2016.
 */
public class TaskCh12N024Test {
    public static void main(String[] args) {
        testFillArrSum();
        testFillArrShift();
    }

    public static void testFillArrSum() {
        int[][] numbers = new int[6][6];
        int[][] resultNums = {
                {1, 1, 1, 1, 1, 1},
                {1, 2, 3, 4, 5, 6},
                {1, 3, 6, 10, 15, 21},
                {1, 4, 10, 20, 35, 56},
                {1, 5, 15, 35, 70, 126},
                {1, 6, 21, 56, 126, 252}};

        assertEquals("TaskCh12N024Test.testFillArrSum", resultNums, fillArrSum(numbers));
        assertEquals("TaskCh12N024Test.testFillArrSum", new int[][]{}, fillArrSum(new int[][]{{1}, {1}}));
    }

    public static void testFillArrShift() {
        int[][] numbers = new int[6][6];
        int[][] resultNums = new int[][]{
                {1, 2, 3, 4, 5, 6},
                {2, 3, 4, 5, 6, 1},
                {3, 4, 5, 6, 1, 2},
                {4, 5, 6, 1, 2, 3},
                {5, 6, 1, 2, 3, 4},
                {6, 1, 2, 3, 4, 5}};

        assertEquals("TaskCh12N024Test.testFillArrShift", resultNums, fillArrShift(numbers));
        assertEquals("TaskCh12N024Test.testFillArrShift", new int[][]{}, fillArrShift(new int[][]{{1}, {1}}));
    }
}
