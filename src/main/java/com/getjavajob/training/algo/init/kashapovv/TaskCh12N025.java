package com.getjavajob.training.algo.init.kashapovv;


import static com.getjavajob.training.algo.util.ConsoleIO.printArr;

/**
 * Created by Вадим on 05.11.2016.
 */
public class TaskCh12N025 {
    public static void main(String[] args) {
        int[][] n = new int[12][10];

        n = fillArrA(n);
        printArr(n, "Array A");
        n = fillArrB(n);
        printArr(n, "Array B");
        n = fillArrV(n);
        printArr(n, "Array V");
        n = fillArrG(n);
        printArr(n, "Array G");

        int[][] d = new int[10][12];
        d = fillArrD(d); // method uses 10x12 dimensional array
        printArr(d, "Array D");

        n = fillArrE(n);
        printArr(n, "Array E");
        n = fillArrJ(n);
        printArr(n, "Array J");
        n = fillArrZ(n);
        printArr(n, "Array Z");
        n = fillArrI(n);
        printArr(n, "Array I");
        n = fillArrK(n);
        printArr(n, "Array K");
        n = fillArrL(n);
        printArr(n, "Array L");
        n = fillArrM(n);
        printArr(n, "Array M");
        n = fillArrN(n);
        printArr(n, "Array N");
        n = fillArrO(n);
        printArr(n, "Array O");
        n = fillArrP(n);
        printArr(n, "Array P");
        n = fillArrR(n);
        printArr(n, "Array R");
    }

    public static int[][] fillArrA(int[][] n) {
        int value = 1;
        for (int i = 0; i < n.length; i++) {
            for (int j = 0; j < n[0].length; j++) {
                n[i][j] = value++;
            }
        }
        return n;
    }

    public static int[][] fillArrB(int[][] n) {
        int value = 1;
        for (int i = 0; i < n[0].length; i++) {
            for (int j = 0; j < n.length; j++) {
                n[j][i] = value++;
            }
        }
        return n;
    }

    public static int[][] fillArrV(int[][] n) {
        int value = 1;
        for (int i = 0; i < n.length; i++) {
            for (int j = n[0].length - 1; j >= 0; j--) {
                n[i][j] = value++;
            }
        }
        return n;
    }

    public static int[][] fillArrG(int[][] n) {
        int value = 1;
        for (int i = 0; i < n[0].length; i++) {
            for (int j = n.length - 1; j >= 0; j--) {
                n[j][i] = value++;
            }
        }
        return n;
    }

    public static int[][] fillArrD(int[][] n) {
        int value = 1;
        for (int i = 0; i < n.length; i++) {
            if ((i + 1) % 2 != 0) {
                for (int j = 0; j < n[0].length; j++) {
                    n[i][j] = value++;
                }
            } else {
                for (int j = n[0].length - 1; j >= 0; j--) {
                    n[i][j] = value++;
                }
            }
        }
        return n;
    }

    public static int[][] fillArrE(int[][] n) {
        int value = 1;
        for (int j = 0; j < n[0].length; j++) {
            if ((j + 1) % 2 != 0) {
                for (int i = 0; i < n.length; i++) {
                    n[i][j] = value++;
                }
            } else {
                for (int i = n.length - 1; i >= 0; i--) {
                    n[i][j] = value++;
                }
            }
        }
        return n;
    }

    public static int[][] fillArrJ(int[][] n) {
        int value = 1;
        for (int i = n.length - 1; i >= 0; i--) {
            for (int j = 0; j < n[0].length; j++) {
                n[i][j] = value++;
            }
        }
        return n;
    }

    public static int[][] fillArrZ(int[][] n) {
        int value = 1;
        for (int j = n[0].length - 1; j >= 0; j--) {
            for (int i = 0; i < n.length; i++) {
                n[i][j] = value++;
            }
        }
        return n;
    }

    public static int[][] fillArrI(int[][] n) {
        int value = 1;
        for (int i = n.length - 1; i >= 0; i--) {
            for (int j = n[0].length - 1; j >= 0; j--) {
                n[i][j] = value++;
            }
        }
        return n;
    }

    public static int[][] fillArrK(int[][] n) {
        int value = 1;
        for (int i = n[0].length - 1; i >= 0; i--) {
            for (int j = n.length - 1; j >= 0; j--) {
                n[j][i] = value++;
            }
        }
        return n;
    }

    public static int[][] fillArrL(int[][] n) {
        int value = 1;
        for (int i = n.length - 1; i >= 0; i--) {
            if ((i + 1) % 2 == 0) {
                for (int j = 0; j < n[0].length; j++) {
                    n[i][j] = value++;
                }
            } else {
                for (int j = n[0].length - 1; j >= 0; j--) {
                    n[i][j] = value++;
                }
            }
        }
        return n;
    }

    public static int[][] fillArrM(int[][] n) {
        int value = 1;
        for (int i = 0; i < n.length; i++) {
            if ((i + 1) % 2 == 0) {
                for (int j = 0; j < n[0].length; j++) {
                    n[i][j] = value++;
                }
            } else {
                for (int j = n[0].length - 1; j >= 0; j--) {
                    n[i][j] = value++;
                }
            }
        }
        return n;
    }

    public static int[][] fillArrN(int[][] n) {
        int value = 1;
        for (int i = n[0].length - 1; i >= 0; i--) {
            if ((i + 1) % 2 == 0) {
                for (int j = 0; j < n.length; j++) {
                    n[j][i] = value++;
                }
            } else {
                for (int j = n.length - 1; j >= 0; j--) {
                    n[j][i] = value++;
                }
            }
        }
        return n;
    }

    public static int[][] fillArrO(int[][] n) {
        int value = 1;
        for (int i = 0; i < n[0].length; i++) {
            if ((i + 1) % 2 == 0) {
                for (int j = 0; j < n.length; j++) {
                    n[j][i] = value++;
                }
            } else {
                for (int j = n.length - 1; j >= 0; j--) {
                    n[j][i] = value++;
                }
            }
        }
        return n;
    }

    public static int[][] fillArrP(int[][] n) {
        int value = 1;
        for (int i = n.length - 1; i >= 0; i--) {
            if ((i + 1) % 2 != 0) {
                for (int j = 0; j < n[0].length; j++) {
                    n[i][j] = value++;
                }
            } else {
                for (int j = n[0].length - 1; j >= 0; j--) {
                    n[i][j] = value++;
                }
            }
        }
        return n;
    }

    public static int[][] fillArrR(int[][] n) {
        int value = 1;
        for (int i = n[0].length - 1; i >= 0; i--) {
            if ((i + 1) % 2 != 0) {
                for (int j = 0; j < n.length; j++) {
                    n[j][i] = value++;
                }
            } else {
                for (int j = n.length - 1; j >= 0; j--) {
                    n[j][i] = value++;
                }
            }
        }
        return n;
    }
}
