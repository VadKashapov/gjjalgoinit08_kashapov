package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

import static com.getjavajob.training.algo.util.ConsoleIO.isEven;
import static com.getjavajob.training.algo.util.ConsoleIO.printArr;

/**
 * Created by Вадим on 05.11.2016.
 */
public class TaskCh12N028 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int size = in.nextInt();
        int[][] nums = new int[size][size];

        nums = fillSpiralArr(nums);
        printArr(nums);
    }

    /**
     * Method fill odd N*N array like clockwise spiral series of increasing numbers by 1;
     **/
    public static int[][] fillSpiralArr(int[][] nums) {
        if (isEven(nums.length) || isEven(nums[0].length)) {
            return new int[][]{};
        }
        int value = 1;
        for (int i = 0; i <= nums.length / 2; i++) {
            for (int j = i; j < nums[0].length - i; j++) {
                nums[i][j] = value++;
            }
            for (int j = i + 1; j < nums.length - i; j++) {
                nums[j][nums.length - 1 - i] = value++;
            }
            for (int j = nums.length - 2 - i; j >= i; j--) {
                nums[nums.length - 1 - i][j] = value++;
            }
            for (int j = nums.length - 2 - i; j > i; j--) {
                nums[j][i] = value++;
            }
        }
        return nums;
    }
}
