package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh12N028.fillSpiralArr;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 09.11.2016.
 */
public class TaskCh12N028Test {
    public static void main(String[] args) {
        testFillSpiralArr();
    }

    public static void testFillSpiralArr() {
        int[][] nums = new int[5][5];
        int[][] res = {
                {1, 2, 3, 4, 5},
                {16, 17, 18, 19, 6},
                {15, 24, 25, 20, 7},
                {14, 23, 22, 21, 8},
                {13, 12, 11, 10, 9}};

        assertEquals("TaskCh12N024Test.testFillSpiralArr", res, fillSpiralArr(nums));
        assertEquals("TaskCh12N024Test.testFillSpiralArr", new int[][]{}, fillSpiralArr(new int[][]{{1, 2}, {1, 2}}));
    }
}
