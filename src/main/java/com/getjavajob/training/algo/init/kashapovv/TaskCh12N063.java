package com.getjavajob.training.algo.init.kashapovv;

import java.util.Arrays;
import java.util.Scanner;

import static com.getjavajob.training.algo.util.ConsoleIO.fillArrayInt;

/**
 * Created by Вадим on 06.11.2016.
 */
public class TaskCh12N063 {
    public static void main(String[] args) {
        int[][] scholarsInClasses = new int[11][4];
        fillArrayInt(scholarsInClasses);

        double[] averScholarCounts = findAverClassesCounts(scholarsInClasses);

        System.out.println(Arrays.toString(averScholarCounts));
    }

    public static double[] findAverClassesCounts(int[][] scholarsInClasses) {
        double[] averScholarCounts = new double[scholarsInClasses.length];

        for (int i = 0; i < scholarsInClasses.length; i++) {
            for (int j = 0; j < scholarsInClasses[i].length; j++) {
                averScholarCounts[i] += scholarsInClasses[i][j];
            }
            averScholarCounts[i] /= scholarsInClasses[i].length;
        }
        return averScholarCounts;
    }
}