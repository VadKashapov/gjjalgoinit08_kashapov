package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh12N063.findAverClassesCounts;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 09.11.2016.
 */
public class TaskCh12N063Test {
    public static void main(String[] args) {
        testFindAverClassesCounts();
    }

    public static void testFindAverClassesCounts() {
        int[][] scholarsInClasses = {
                {20, 20, 20, 20},
                {24, 24, 24, 24},
                {26, 26, 30, 30},
                {20, 21, 22, 23},
                {20, 21, 22, 23},
                {20, 21, 22, 23},
                {20, 21, 22, 23},
                {20, 21, 22, 23},
                {20, 21, 22, 23},
                {20, 21, 22, 23},
                {20, 21, 22, 23}};
        double[] averScholarCounts = {20, 24, 28, 21.5, 21.5, 21.5, 21.5, 21.5, 21.5, 21.5, 21.5};
        assertEquals("TaskCh12N063Test.testFindAverClassCount", averScholarCounts,
                findAverClassesCounts(scholarsInClasses));
    }
}
