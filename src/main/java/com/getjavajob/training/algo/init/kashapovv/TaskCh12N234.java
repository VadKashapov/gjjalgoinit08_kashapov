package com.getjavajob.training.algo.init.kashapovv;

import java.util.Scanner;

import static com.getjavajob.training.algo.util.ConsoleIO.fillArrayInt;
import static com.getjavajob.training.algo.util.ConsoleIO.printArr;
import static java.lang.System.arraycopy;

/**
 * Created by Вадим on 06.11.2016.
 */
public class TaskCh12N234 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int columnSize = in.nextInt();
        int rowSize = in.nextInt();

        int[][] delRowNums = new int[columnSize][rowSize];
        fillArrayInt(delRowNums);

        int kIndex = in.nextInt();
        int[][] arrDelColumn = delRow(delRowNums, kIndex);
        printArr(arrDelColumn, "Delete column");

        int[][] delColNums = new int[columnSize][rowSize];
        fillArrayInt(delColNums);

        int sIndex = in.nextInt();
        int[][] arrDelRow = delColumn(delColNums, sIndex);
        printArr(arrDelRow, "Delete row");
    }

    public static int[][] delRow(int[][] nums, int kIndex) {
        if (kIndex < 0 || kIndex > nums.length) {
            return new int[][]{};
        }
        for (int i = kIndex - 1; i < nums.length - 1; i++) {
            arraycopy(nums[i + 1], 0, nums[i], 0, nums[0].length);
        }
        for (int j = 0; j < nums[0].length; j++) {
            nums[nums.length - 1][j] = 0;
        }
        return nums;
    }

    public static int[][] delColumn(int[][] nums, int sIndex) {
        if (sIndex < 0 || sIndex > nums.length) {
            return new int[][]{};
        }
        for (int j = sIndex - 1; j < nums[0].length - 1; j++) {
            for (int i = 0; i < nums.length; i++) {
                nums[i][j] = nums[i][j + 1];
            }
        }
        for (int i = 0; i < nums.length; i++) {
            nums[i][nums[0].length - 1] = 0;
        }
        return nums;
    }
}