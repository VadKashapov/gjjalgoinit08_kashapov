package com.getjavajob.training.algo.init.kashapovv;

import static com.getjavajob.training.algo.init.kashapovv.TaskCh12N234.delColumn;
import static com.getjavajob.training.algo.init.kashapovv.TaskCh12N234.delRow;
import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 09.11.2016.
 */
public class TaskCh12N234Test {
    public static void main(String[] args) {
        testDelColumn();
        testDelRow();
    }

    public static void testDelColumn() {
        int[][] n = {
                {1, 1, 1, 1, 1},
                {2, 2, 2, 2, 2},
                {3, 3, 3, 3, 3},
                {4, 4, 4, 4, 4},
                {5, 5, 5, 5, 5},
                {6, 6, 6, 6, 6}};
        int[][] res = {
                {1, 1, 1, 1, 1},
                {2, 2, 2, 2, 2},
                {4, 4, 4, 4, 4},
                {5, 5, 5, 5, 5},
                {6, 6, 6, 6, 6},
                {0, 0, 0, 0, 0}};

        assertEquals("TaskCh12N234Test.testDelColumn", res, delRow(n, 3));
        assertEquals("TaskCh12N234Test.testDelColumn", new int[][]{}, delRow(n, -5));
        assertEquals("TaskCh12N234Test.testDelColumn", new int[][]{}, delRow(n, 100));
    }

    public static void testDelRow() {
        int[][] n = {
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5},
                {1, 2, 3, 4, 5}};
        int[][] res = {
                {1, 2, 3, 5, 0},
                {1, 2, 3, 5, 0},
                {1, 2, 3, 5, 0},
                {1, 2, 3, 5, 0},
                {1, 2, 3, 5, 0},
                {1, 2, 3, 5, 0}};

        assertEquals("TaskCh12N234Test.testDelRow", res, delColumn(n, 4));
        assertEquals("TaskCh12N234Test.testDelRow", new int[][]{}, delColumn(n, -6));
        assertEquals("TaskCh12N234Test.testDelRow", new int[][]{}, delColumn(n, 100));
    }
}
