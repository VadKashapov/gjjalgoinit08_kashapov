package com.getjavajob.training.algo.init.kashapovv;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

import static com.getjavajob.training.algo.util.ConsoleIO.println;
import static java.util.Calendar.getInstance;

/**
 * Created by Вадим on 06.11.2016.
 */
public class TaskCh13N012 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        List<Employee> employees = new ArrayList<>();
        for (int i = 20; i > 0; i--) {
            println("Enter name: ");
            String name = in.next();
            println("Enter surname: ");
            String surname = in.next();
            println("Enter middle name: ");
            String middleName = in.next();
            println("Enter address: ");
            String address = in.next();
            println("Enter enrolled month : ");
            int jobMonth = in.nextInt();
            println("Enter enrolled year (job year <= current year):");
            int jobYear = in.nextInt();
            employees.add(new Employee(name, middleName, surname, address, jobMonth, jobYear));
        }
        println("Enter years count:");
        int years = in.nextInt();
        Database base = new Database(employees);

        List<Employee> employeesByYears = base.findEmployeeByYears(years);
        System.out.printf("List of employees who worked more %d years:%n", years);
        print(employeesByYears);

        String name = in.next();
        List<Employee> employeesByName = base.findEmployeeByName(name);
        System.out.printf("List of employees that contain name \"%s\" :%n", name);
        print(employeesByName);
    }

    public static void print(List<Employee> employees) {
        for (Employee e : employees) {
            System.out.printf("%s%s %s %s %s %s%n", e.getFirstName(),
                    e.getMiddleName() != null ? ' ' + e.getMiddleName() : "", e.getSurName(),
                    e.getAdress(), e.getJobMonth(), e.getJobYear());
        }
    }
}

class Database {
    private List<Employee> employees;

    public Database(List<Employee> employees) {
        this.employees = new ArrayList<>();
        for (Employee e : employees) {
            this.employees.add(new Employee(e.getFirstName(), e.getMiddleName(), e.getSurName(),
                    e.getAdress(), e.getJobMonth(), e.getJobYear()));
        }
    }

    public List<Employee> findEmployeeByYears(int years) {
        int todayYear = getInstance().get(Calendar.YEAR);
        int todayMonth = getInstance().get(Calendar.MONTH);
        List<Employee> employeesByYears = new ArrayList<>();
        for (Employee employee : employees) {
            if (employee.yearsWork(todayMonth, todayYear) >= years) {
                employeesByYears.add(employee);
            }
        }
        return employeesByYears;
    }

    public List<Employee> findEmployeeByName(String name) {
        List<Employee> employeesByName = new ArrayList<>();
        for (Employee employee : employees) {
            if (employee.getFirstName().toLowerCase().contains(name.toLowerCase())
                    || (employee.getMiddleName() != null
                    && employee.getMiddleName().toLowerCase().contains(name.toLowerCase()))
                    || (employee.getSurName().toLowerCase().contains(name.toLowerCase()))) {
                employeesByName.add(employee);
            }
        }
        return employeesByName;
    }
}

class Employee {
    private String firstName;
    private String middleName;
    private String surName;
    private String address;
    private int jobMonth;
    private int jobYear;

    public Employee(String firstName, String middleName, String surName, String address, int jobMonth, int jobYear) {
        setFirstName(firstName);
        setMiddleName(middleName);
        setSurName(surName);
        setAddress(address);
        setJobMonth(jobMonth);
        setJobYear(jobYear);
    }


    public Employee(String firstName, String surName, String address, int jobMonth, int jobYear) {
        this(firstName, "", surName, address, jobMonth, jobYear);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (firstName != null ? firstName.hashCode() : 0);
        result = prime * result + (middleName != null ? middleName.hashCode() : 0);
        result = prime * result + (surName != null ? surName.hashCode() : 0);
        result = prime * result + (address != null ? address.hashCode() : 0);
        result = prime * result + jobMonth;
        result = prime * result + jobYear;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Employee emp = (Employee) obj;
        if (!firstName.equals(emp.firstName) || middleName != null && !middleName.equals(emp.middleName)
                || !surName.equals(emp.surName) || !address.equals(emp.address)
                || jobMonth != emp.jobMonth || jobYear != emp.jobYear) {
            return false;
        }
        return true;
    }

    public int yearsWork(int todayMonth, int todayYear) {
        int jobMonth = getJobMonth();
        int jobYear = getJobYear();
        return todayMonth >= jobMonth ? todayYear - jobYear : todayYear - jobYear - 1;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getAdress() {
        return address;
    }

    public void setAddress(String adress) {
        this.address = adress;
    }


    public int getJobMonth() {
        return jobMonth;
    }

    public void setJobMonth(int jobMonth) {
        this.jobMonth = jobMonth;
    }

    public int getJobYear() {
        return jobYear;
    }

    public void setJobYear(int jobYear) {
        this.jobYear = jobYear;
    }
}
