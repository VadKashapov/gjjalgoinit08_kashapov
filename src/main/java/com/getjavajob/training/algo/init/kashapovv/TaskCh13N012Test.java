package com.getjavajob.training.algo.init.kashapovv;

import java.util.ArrayList;
import java.util.List;

import static com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Вадим on 09.11.2016.
 */
public class TaskCh13N012Test {
    public static void main(String[] args) {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Vadim", "Flurovich", "Kashapov", "Izmaylovskaya 73/2", 9, 2012));
        employees.add(new Employee("Dmitrii", "Flurovich", "Kashapov", "Izmaylovskaya 73/2", 6, 2013));
        employees.add(new Employee("Ruslan", "Kashapov", "Izmaylovskaya 73/2", 8, 2014));
        employees.add(new Employee("Vladimir", "Kashapov", "Izmaylovskaya 73/2", 8, 2014));
        employees.add(new Employee("Mihail", "Kashapov", "Izmaylovskaya 73/2", 8, 2014));
        employees.add(new Employee("Ivan", "Kashapov", "Izmaylovskaya 73/2", 8, 2014));
        employees.add(new Employee("Sergey", "Kashapov", "Izmaylovskaya 73/2", 8, 2014));
        employees.add(new Employee("Konstantin", "Kashapov", "Izmaylovskaya 73/2", 8, 2014));
        employees.add(new Employee("Aleksey", "Kashapov", "Izmaylovskaya 73/2", 8, 2014));
        employees.add(new Employee("Ruslan", "Bunin", "Izmaylovskaya 73/2", 8, 2014));
        employees.add(new Employee("Vladimir", "Sidorov", "Izmaylovskaya 73/2", 8, 2014));
        employees.add(new Employee("Vladimir", "Egorov", "Izmaylovskaya 73/2", 8, 2014));
        employees.add(new Employee("Vladimir", "Shmidt", "Izmaylovskaya 73/2", 8, 2014));
        employees.add(new Employee("Vladimir", "Elizarov", "Izmaylovskaya 73/1", 8, 2014));
        employees.add(new Employee("Vladimir", "Komarov", "Izmaylovskaya 73/2", 8, 2014));
        employees.add(new Employee("Vladimir", "Kozlov", "Izmaylovskaya 70/1", 8, 2014));
        employees.add(new Employee("Vladislav", "Kashapov", "Izmaylovskaya 71/1", 8, 2014));
        employees.add(new Employee("Ilmir", "Kashapov", "Izmaylovskaya 72/1", 8, 2014));
        employees.add(new Employee("Vladimir", "Kashapov", "Partizanskay 26", 8, 2014));
        employees.add(new Employee("Vladimir", "Kashapov", "Partizanskay 27", 8, 2014));
        employees.add(new Employee("Vladimir", "Kashapov", "Partizanskay 28", 8, 2014));

        Database base = new Database(employees);

        testFindEmployeeByYears(base);
        testFindEmployeeByName(base);
    }


    public static void testFindEmployeeByYears(Database base) {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Vadim", "Flurovich", "Kashapov", "Izmaylovskaya 73/2", 9, 2012));
        employees.add(new Employee("Dmitrii", "Flurovich", "Kashapov", "Izmaylovskaya 73/2", 6, 2013));

        assertEquals("TaskCh13N012Test.testFindEmployeeByYears", employees, base.findEmployeeByYears(3));
    }

    public static void testFindEmployeeByName(Database base) {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee("Ruslan", "Kashapov", "Izmaylovskaya 73/2", 8, 2014));
        employees.add(new Employee("Ruslan", "Bunin", "Izmaylovskaya 73/2", 8, 2014));

        assertEquals("TaskCh13N012Test.testFindEmployeeByName", employees, base.findEmployeeByName("ruslan"));
    }
}
