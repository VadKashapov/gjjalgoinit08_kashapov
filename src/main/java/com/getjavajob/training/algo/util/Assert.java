package com.getjavajob.training.algo.util;

import com.getjavajob.training.algo.init.kashapovv.TaskCh13N012;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Вадим on 28.10.2016.
 */
public class Assert {
    public static void assertEquals(String testName, int expected, int actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + "failed: expected " + expected + ",actual " + actual);
        }
    }

    public static void assertEquals(String testName, double expected, double actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, String expected, String actual) {
        if (expected.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, double expected, int index, double[] actualArray) {
        if (expected == actualArray[index - 1]) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actualArray[index - 1]);
        }
    }

    public static void assertEquals(String testName, int[] expected, int[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed:\n Expected: " + Arrays.toString(expected)
                    + "\nActual: " + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, int[][] expected, int[][] actual) {
        if (Arrays.deepEquals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed:\n Expected: " + Arrays.deepToString(expected)
                    + "\nActual: " + Arrays.deepToString(actual));
        }
    }

    public static void assertEquals(String testName, double[] expected, double[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed:\n Expected: " + Arrays.toString(expected)
                    + "\nActual: " + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, List expected, List actual) {
        if (expected.equals(actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed:\nExpected: ");
            TaskCh13N012.print(expected);
            System.out.println("Actual: ");
            TaskCh13N012.print(actual);
        }
    }
}
