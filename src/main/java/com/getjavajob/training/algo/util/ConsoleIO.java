package com.getjavajob.training.algo.util;

import java.util.Scanner;

/**
 * Created by Вадим on 18.11.2016.
 */
public class ConsoleIO {
    private static Scanner in;

    public static int readConsoleInt() {
        in = new Scanner(System.in);
        return in.nextInt();
    }

    public static void fillArrayInt(int[] input) {

        in = new Scanner(System.in);
        for (int i = 0; i < input.length; i++) {
            input[i] = in.nextInt();
        }
    }

    public static void fillArrayInt(int[][] input) {

        in = new Scanner(System.in);
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input[i].length; j++) {
                input[i][j] = in.nextInt();
            }
        }
    }

    public static double readConsoleDouble() {
        in = new Scanner(System.in);
        return in.nextDouble();
    }

    public static void fillArrayDouble(double[][] input) {

        in = new Scanner(System.in);
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input[i].length; j++) {
                input[i][j] = in.nextDouble();
            }
        }
    }

    public static String readConsoleString() {
        in = new Scanner(System.in);
        return in.next();
    }

    public static boolean isNatural(int value) {
        return value > 0;
    }

    public static boolean isEven(int value) {
        return value % 2 == 0;
    }

    public static void println(String string) {
        System.out.println(string);
    }

    public static void print(int[] values) {
        for (int value : values) {
            System.out.print(" " + value);
        }
    }

    public static void print(int[][] values) {
        for (int[] i : values) {
            for (int j : i) {
                System.out.print(j + " ");
            }
            System.out.print('\n');
        }
        System.out.print('\n');
    }

    public static void printArr(int[][] n, String name) {
        System.out.println("_________________" + name + "__________________");
        for (int[] i : n) {
            for (int j : i) {
                System.out.printf("%4d", j);
            }
            System.out.println();
        }
    }

    public static void printArr(int[][] n) {
        for (int[] i : n) {
            for (int j : i) {
                System.out.printf("%3d", j);
            }
            System.out.println();
        }
    }
}
